# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2023/7/25 13:59

import mysql.connector
import configparser
from datetime import datetime

config = configparser.ConfigParser()
config.read('db.conf')

my_host = config.get('adbp', 'host')
my_database = config.get('adbp', 'database')
my_user = config.get('adbp', 'user')
my_password = config.get('adbp', 'password')

con = mysql.connector.connect(
    host=my_host,
    user=my_user,
    password=my_password,
    database=my_database
)
cu_time = datetime.now()
cu_date = cu_time.date()
loop_time = datetime.now()

try:
    cur = con.cursor(dictionary=True)

    # 物料流水表
    sql_material_dro = 'DROP TABLE IF EXISTS ods_whcenter.wh_stockagedetail;'
    sql_material_cre = 'CREATE TABLE ods_whcenter.wh_stockagedetail( ' \
                       'Id bigint auto_increment primary key , ' \
                       'StockId            varchar, ' \
                       'StockNo            varchar, ' \
                       'FromWarehouseId    varchar, ' \
                       'FromWarehouseName  varchar, ' \
                       'SupplierName       varchar, ' \
                       'MainPartId         varchar, ' \
                       'MainPartName       varchar, ' \
                       'WarehouseId        varchar, ' \
                       'MaterialId         varchar, ' \
                       'MaterialName       varchar, ' \
                       'MaterialType       varchar, ' \
                       'MaterialTypeCode   varchar, ' \
                       'InOrOut            int default 0, ' \
                       'AuditState         int default 0, ' \
                       'StockType          varchar, ' \
                       'OwnerId            varchar, ' \
                       'OwnerName          varchar, ' \
                       'OwnerShortName     varchar, ' \
                       'AuditTime          timestamp, ' \
                       'StockNum           int, ' \
                       'StockPrice         decimal(18, 4), ' \
                       'TaxPoint           int ' \
                       ')DISTRIBUTE BY HASH(id) INDEX_ALL=\'Y\' STORAGE_POLICY=\'HOT\' ' \
                       'ENGINE=\'XUANWU\' TABLE_PROPERTIES=\'{"format":"columnstore"}\';'
    cur.execute(sql_material_dro)
    cur.execute(sql_material_cre)
    # 获取货主库存
    sql_stock_dro = 'DROP TABLE IF EXISTS ods_whcenter.tm_ownerstockinfo;'
    sql_stock_cre = 'CREATE TABLE ods_whcenter.tm_ownerstockinfo ( ' \
                    'Id bigint NOT NULL AUTO_INCREMENT COMMENT \'自增主键\', ' \
                    'WarehouseId varchar COMMENT \'仓库Id(tb_warehouse.Id)\', ' \
                    'MaterialId varchar COMMENT \'物料Id\', ' \
                    'OwnerId varchar COMMENT \'货主Id\', ' \
                    'StockNum int COMMENT \'在库数量\', ' \
                    'RealityNum int COMMENT \'可用库存\', ' \
                    'primary key (id) ' \
                    ') DISTRIBUTE BY HASH(id) INDEX_ALL=\'Y\' STORAGE_POLICY=\'HOT\' ' \
                    'ENGINE=\'XUANWU\' BLOCK_SIZE=8192 TABLE_PROPERTIES=\'{"format":"columnstore"}\';'
    cur.execute(sql_stock_dro)
    cur.execute(sql_stock_cre)
    sql_stock_ins = 'INSERT INTO ods_whcenter.tm_ownerstockinfo(Id, WarehouseId, MaterialId, ' \
                    'OwnerId, StockNum, RealityNum) SELECT Id, WarehouseId, MaterialId, OwnerId, StockNum, ' \
                    'RealityNum FROM ods_whcenter.tb_ownerstockinfo ' \
                    'WHERE IsCodeSingle = 0 AND StockNum <> 0 AND Deleted = 0;'

    # 写入物料流水数据
    sql_instock_ins = 'INSERT INTO ods_whcenter.wh_stockagedetail (StockId,StockNo,FromWarehouseId,' \
                      'FromWarehouseName,MainPartId,MainpartName,WarehouseId,MaterialId,MaterialName,MaterialType, ' \
                      'MaterialTypeCode,InOrOut,AuditState,StockType,OwnerId,OwnerName,OwnerShortName, ' \
                      'AuditTime,StockNum,StockPrice,TaxPoint)SELECT a.Id,a.InStockNo,a.FromWarehouseId, ' \
                      'a.FromWarehouseName,a.MainPartId,e.Name AS MainpartName,a.WarehouseId, ' \
                      'b.MaterialId,b.MaterialName,b.MaterialType,b.MaterialTypeCode,0 AS InOrOut, ' \
                      'a.AuditState,a.InStockType AS StockType,IFNULL(c.OwnerId,b.OwnerId) AS OwnerId, ' \
                      'f.Name AS OwnerName,f.ShortName AS OwnerShortName,a.AuditTime, ' \
                      'COALESCE(d.StockNum,c.InStockNum,b.InStockedNum) AS StockNum,d.StockPrice, ' \
                      'd.TaxPoint FROM ods_whcenter.tb_instockinfo a ' \
                      'INNER JOIN ods_whcenter.tb_instockdetail b ON b.InStockId=a.Id ' \
                      'AND b.Deleted=0 AND b.IsCodeSingle=0 ' \
                      'LEFT JOIN ods_whcenter.tb_instockdetailextra c ON c.InStockDetailId=b.Id ' \
                      'AND c.Deleted=0 LEFT JOIN ods_whcenter.tb_purchasedetailextra d ' \
                      'ON d.StockDetailId=b.Id AND d.Deleted=0 LEFT JOIN ods_whcenter.tb_mainpartinfo e ' \
                      'ON e.ProductCenterId=a.MainPartId AND e.Deleted=0 ' \
                      'LEFT JOIN ods_whcenter.tb_ownerinfo f ON f.Id=IFNULL(c.OwnerId,b.OwnerId) ' \
                      'AND f.Deleted=0 WHERE a.AuditState IN (0,2) AND a.Deleted=0 UNION ' \
                      'SELECT ah.Id,ah.InStockNo,ah.FromWarehouseId,ah.FromWarehouseName,ah.MainPartId, ' \
                      'eh.Name AS MainpartName,ah.WarehouseId,bh.MaterialId,bh.MaterialName, ' \
                      'bh.MaterialType,bh.MaterialTypeCode,0 AS InOrOut,ah.AuditState,ah.InStockType ' \
                      'AS StockType,IFNULL(ch.OwnerId,bh.OwnerId) AS OwnerId,fh.Name AS OwnerName, ' \
                      'fh.ShortName AS OwnerShortName,ah.AuditTime,COALESCE(dh.StockNum,ch.InStockNum, ' \
                      'bh.InStockedNum) AS StockNum,dh.StockPrice,dh.TaxPoint ' \
                      'FROM ods_whcenter.tb_instockinfohis ah ' \
                      'INNER JOIN ods_whcenter.tb_instockdetailhis bh ON bh.InStockId=ah.Id ' \
                      'AND bh.Deleted=0 AND bh.IsCodeSingle=0  ' \
                      'LEFT JOIN ods_whcenter.tb_instockdetailextra ch ' \
                      'ON ch.InStockDetailId=bh.Id AND ch.Deleted=0 ' \
                      'LEFT JOIN ods_whcenter.tb_purchasedetailextra dh ON dh.StockDetailId=bh.Id ' \
                      'AND dh.Deleted=0 LEFT JOIN ods_whcenter.tb_mainpartinfo eh ' \
                      'ON eh.ProductCenterId=ah.MainPartId AND eh.Deleted=0 ' \
                      'LEFT JOIN ods_whcenter.tb_ownerinfo fh ON fh.Id=IFNULL(ch.OwnerId,bh.OwnerId) ' \
                      'AND fh.Deleted=0 WHERE ah.AuditState = 2 AND ah.Deleted=0 ORDER BY AuditTime DESC;'
    sql_outstock_ins = 'INSERT INTO ods_whcenter.wh_stockagedetail (StockId,StockNo,MainPartId,MainpartName, ' \
                       'WarehouseId,MaterialId,MaterialName,MaterialType,MaterialTypeCode,InOrOut,AuditState, ' \
                       'StockType,OwnerId,OwnerName,OwnerShortName,AuditTime,StockNum,StockPrice,TaxPoint) ' \
                       'SELECT ao.Id,ao.OutStockNo,ao.MainPartId,eo.Name AS MainpartName,ao.WarehouseId, ' \
                       'bo.MaterialId,bo.MaterialName,bo.MaterialType,bo.MaterialTypeCode,1 AS InOrOut, ' \
                       'ao.AuditState,ao.OutStockType AS StockType,IFNULL(co.OwnerId,bo.OwnerId) ' \
                       'AS OwnerId,fo.Name AS OwnerName,fo.ShortName AS OwnerShortName,ao.AuditTime,' \
                       'IFNULL(co.OutStockNum,bo.OutStockedNum)AS StockNum,NULL AS StockPrice,' \
                       'NULL AS TaxPoint FROM ods_whcenter.tb_outstockinfo ao ' \
                       'INNER JOIN ods_whcenter.tb_outstockdetail bo ON bo.OutStockId=ao.Id AND bo.Deleted=0 ' \
                       'AND bo.IsCodeSingle=0 LEFT JOIN ods_whcenter.tb_outstockdetailextra co ' \
                       'ON co.OutStockDetailId=bo.Id AND co.Deleted=0 ' \
                       'LEFT JOIN ods_whcenter.tb_mainpartinfo eo ' \
                       'ON eo.ProductCenterId=ao.MainPartId AND eo.Deleted=0 ' \
                       'LEFT JOIN ods_whcenter.tb_ownerinfo fo ON fo.Id = IFNULL(co.OwnerId,bo.OwnerId) ' \
                       'AND fo.Deleted=0 WHERE ao.AuditState IN (3,4) AND ao.Deleted=0 UNION ' \
                       'SELECT aoh.Id,aoh.OutStockNo,aoh.MainPartId,eoh.Name AS MainpartName,aoh.WarehouseId, ' \
                       'boh.MaterialId,boh.MaterialName,boh.MaterialType,boh.MaterialTypeCode,1 AS InOrOut, ' \
                       'aoh.AuditState,aoh.OutStockType AS StockType,IFNULL(coh.OwnerId,boh.OwnerId) AS OwnerId,' \
                       'foh.Name AS OwnerName,foh.ShortName AS OwnerShortName,aoh.AuditTime,' \
                       'IFNULL(coh.OutStockNum,boh.OutStockedNum) AS StockNum, NULL AS StockPrice,NULL AS TaxPoint ' \
                       'FROM ods_whcenter.tb_outstockinfohis aoh ' \
                       'INNER JOIN ods_whcenter.tb_outstockdetailhis boh ON boh.OutStockId=aoh.Id ' \
                       'AND boh.Deleted=0 AND boh.IsCodeSingle=0 ' \
                       'LEFT JOIN ods_whcenter.tb_outstockdetailextra coh ON coh.OutStockDetailId=boh.Id ' \
                       'AND coh.Deleted=0 LEFT JOIN ods_whcenter.tb_mainpartinfo eoh ON eoh.ProductCenterId= ' \
                       'aoh.MainPartId AND eoh.Deleted=0 LEFT JOIN ods_whcenter.tb_ownerinfo foh ' \
                       'ON foh.Id=IFNULL(coh.OwnerId,boh.OwnerId) AND foh.Deleted=0 WHERE aoh.AuditState = 4 ' \
                       'AND aoh.Deleted=0 ORDER BY AuditTime DESC;'
    cur.execute(sql_instock_ins)
    cur.execute(sql_outstock_ins)
    cur.execute(sql_stock_ins)

    # 填充采购价格、税率、供应商
    # 无税率、价格的，继承最近一次有价格、税率的，以价格为空优先取
    sql_find_st = 'SELECT d.Name AS SupplierName, ' \
                  'IFNULL(c.StockPrice,b.StockPrice) AS StockPrice, ' \
                  'IFNULL(c.TaxPoint,b.TaxPoint) AS TaxPoint ' \
                  'FROM ods_whcenter.tb_purchaseinfo a ' \
                  'INNER JOIN ods_whcenter.tb_purchasedetail b ' \
                  'ON b.PurchaseId=a.Id ' \
                  'AND b.MaterialId=%s ' \
                  'AND b.Deleted=0 ' \
                  'LEFT JOIN ods_whcenter.tb_purchasedetailextra c ' \
                  'ON c.PurchaseDetailId=b.Id ' \
                  'AND c.Deleted=0 ' \
                  'LEFT JOIN ods_whcenter.tb_supplier d ' \
                  'ON d.Id=b.SupplierId ' \
                  'AND d.Deleted=0 ' \
                  'WHERE a.Deleted=0 AND a.PurchaseType IN (\'CG0\',\'CG3\') ' \
                  'AND a.AuditTime <= %s ' \
                  'AND a.AuditState IN (3,4,8) ' \
                  'ORDER BY a.AuditTime DESC LIMIT 1;'
    sql_no_st = 'SELECT * FROM ods_whcenter.wh_stockagedetail WHERE SupplierName IS NULL LIMIT 1;'
    sql_de_st = 'DELETE FROM ods_whcenter.wh_stockagedetail WHERE Id = %s;'
    sql_in_st = 'INSERT INTO ods_whcenter.wh_stockagedetail (Id,StockId,StockNo,FromWarehouseId,FromWarehouseName,' \
                'SupplierName,MainPartId,MainPartName,WarehouseId,MaterialId,MaterialName,MaterialType,' \
                'MaterialTypeCode,InOrOut,AuditState,StockType,OwnerId,OwnerName,OwnerShortName,AuditTime,' \
                'StockNum,StockPrice,TaxPoint) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, ' \
                '%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'

    # 循环更新
    cur.execute(sql_no_st)
    pr = cur.fetchall()
    while pr:
        pr_id = pr[0].get('Id')
        pr_stockid = pr[0].get('StockId')
        pr_stockno = pr[0].get('StockNo')
        pr_stockid = pr[0].get('StockId')
        pr_fromwarehouseid = pr[0].get('FromWarehouseId')
        pr_fromwarehousename = pr[0].get('FromWarehouseName')
        pr_mainpartid = pr[0].get('MainPartId')
        pr_mainpartname = pr[0].get('MainPartName')
        pr_warehouseid = pr[0].get('WarehouseId')
        pr_materialid = pr[0].get('MaterialId')
        pr_materialname = pr[0].get('MaterialName')
        pr_materialtype = pr[0].get('MaterialType')
        pr_materialtypecode = pr[0].get('MaterialTypeCode')
        pr_inorout = pr[0].get('InOrOut')
        pr_auditstate = pr[0].get('AuditState')
        pr_stocktype = pr[0].get('StockType')
        pr_ownerid = pr[0].get('OwnerId')
        pr_ownername = pr[0].get('OwnerName')
        pr_ownershortname = pr[0].get('OwnerShortName')
        pr_audittime = pr[0].get('AuditTime')
        pr_stocknum = pr[0].get('StockNum')
        pr_stockprice = pr[0].get('StockPrice')
        pr_taxpoint = pr[0].get('TaxPoint')

        cur.execute(sql_find_st, [pr_materialid, pr_audittime])
        pu = cur.fetchall()
        if pu:
            pu_sn = pu[0].get('SupplierName')
            pu_sp = pu[0].get('StockPrice')
            pu_tx = pu[0].get('TaxPoint')
            if pr_stockprice is None:
                pr_stockprice = pu_sp
                pr_taxpoint = pu_tx
            cur.execute(sql_de_st, [pr_id])
            cur.execute(sql_in_st, [pr_id, pr_stockid, pr_stockno, pr_fromwarehouseid, pu_sn,
                                    pr_fromwarehousename, pr_mainpartid, pr_mainpartname, pr_warehouseid,
                                    pr_materialid, pr_materialname, pr_materialtype, pr_materialtypecode,
                                    pr_inorout, pr_auditstate, pr_stocktype, pr_ownerid, pr_ownername,
                                    pr_ownershortname, pr_audittime, pr_stocknum, pr_stockprice, pr_taxpoint])
            con.commit()
        cur.execute(sql_no_st)
        pr = cur.fetchall()

    # 创建每条单据计算临时表
    sql_sto_dro = 'DROP TABLE IF EXISTS ods_whcenter.wh_stockagecur;'
    sql_sto_cre = 'CREATE TABLE ods_whcenter.wh_stockagecur (' \
                  'Id bigint unsigned NOT NULL AUTO_INCREMENT COMMENT \'自增主键\' PRIMARY KEY, ' \
                  'StockNo varchar COMMENT \'出入库单号\', ' \
                  'MainPartId varchar COMMENT \'销售平台Id\', ' \
                  'MainPartName varchar COMMENT \'销售平台名称\', ' \
                  'OwnerId varchar COMMENT \'货主Id\', ' \
                  'OwnerName varchar COMMENT \'货主名称\', ' \
                  'OwnerShortName varchar COMMENT \'货主简称\', ' \
                  'StockType varchar COMMENT \'出入库类型\', ' \
                  'WarehouseId varchar COMMENT \'所在二级仓Id\', ' \
                  'FromWarehouseId varchar COMMENT \'所在二级仓Id\', ' \
                  'MaterialId varchar COMMENT \'物料Id\', ' \
                  'MaterialName varchar COMMENT \'物料名称\', ' \
                  'MaterialType varchar COMMENT \'物料类型\', ' \
                  'MaterialTypeCode varchar COMMENT \'物料类型编码\', ' \
                  'IsTransport smallint COMMENT \'是否在途\', ' \
                  'StockPrice decimal(18,2) COMMENT \'采购单价\', ' \
                  'TaxPoint int COMMENT \'采购税率\', ' \
                  'FnStockAge int COMMENT \'财务管理库龄\', ' \
                  'FnStockTime timestamp COMMENT \'财务库龄入库时间\', ' \
                  'StockAge int COMMENT \'仓库库龄\', ' \
                  'StockTime datetime COMMENT \'仓库库龄入库时间\', ' \
                  'StockNum int) ' \
                  'DISTRIBUTE BY HASH(id) INDEX_ALL=\'Y\' STORAGE_POLICY=\'HOT\' ' \
                  'ENGINE=\'XUANWU\' TABLE_PROPERTIES=\'{"format":"columnstore"}\';'
    cur.execute(sql_sto_dro)
    cur.execute(sql_sto_cre)

    # 分组获取数据，来每条循环
    sql_group = 'SELECT DISTINCT a.WarehouseId, a.MaterialId, a.OwnerId, a.StockPrice, a.TaxPoint ' \
                'FROM ods_whcenter.wh_stockagedetail a WHERE EXISTS(SELECT 1 FROM tm_ownerstockinfo b ' \
                'WHERE b.MaterialId=a.MaterialId AND b.WarehouseId=a.WarehouseId AND b.OwnerId=a.OwnerId);'
    cur.execute(sql_group)
    rows = cur.fetchall()
    for row in rows:
        r_id = row.get('Id')
        r_wi = row.get('WarehouseId')
        r_mi = row.get('MaterialId')
        r_oi = row.get('OwnerId')
        r_sp = row.get('StockPrice')
        r_tp = row.get('TaxPoint')

        # 获取仓-物料的当前库内库存
        sql_cur_stock = 'SELECT WarehouseId, MaterialId, StockNum, RealityNum ' \
                        'FROM ods_whcenter.tm_ownerstockinfo;'
        cur.execute(sql_cur_stock)
        cu_sn = cur.fetchall()

        if len(cu_sn) > 0:
            cu_sn = int(cu_sn[0].get('StockNum'))
            mi_num = 0
            need_num = cu_sn
            print('当前在库库存： {}'.format(cu_sn))

            # 逐条获取分组后的每条物料
            sql_search = 'SELECT * ' \
                         'FROM ods_whcenter.wh_stockagedetail a ' \
                         'WHERE 1=1 '
            params = []
            sql_search += 'AND a.WarehouseId = %s '
            params.append(r_wi)
            sql_search += 'AND a.MaterialId = %s '
            params.append(r_mi)
            sql_search += 'AND a.OwnerId = %s '
            params.append(r_oi)
            sql_search += 'AND NOT EXISTS(SELECT 1 FROM ods_whcenter.wh_stockagecur b WHERE b.Id=a.Id) ' \
                          'ORDER BY a.AuditTime DESC, a.StockPrice DESC;'
            print('当前处理物料："' + r_mi + '" ,仓库："' + r_wi + '",货主："' + str(r_oi) + '",' + '行号：' + str(r_id))
            cur.execute(sql_search, params)
            res_sin = cur.fetchall()
            for res_row in res_sin:
                if need_num > 0:
                    res_id = res_row.get('Id')
                    res_sno = res_row.get('StockNo')
                    res_fwi = res_row.get('FromWarehouseId')
                    res_mpi = res_row.get('MainPartId')
                    res_mpn = res_row.get('MainPartName')
                    res_oi = res_row.get('OwnerId')
                    res_on = res_row.get('OwnerName')
                    res_osn = res_row.get('OwnerShortName')
                    res_wi = res_row.get('WarehouseId')
                    res_mi = res_row.get('MaterialId')
                    res_mn = res_row.get('MaterialName')
                    res_mt = res_row.get('MaterialType')
                    res_mtc = res_row.get('MaterialTypeCode')
                    res_io = res_row.get('InOrOut')
                    res_as = res_row.get('AuditState')
                    res_st = res_row.get('StockType')
                    res_at = res_row.get('AuditTime')
                    res_sn = res_row.get('StockNum')
                    res_mp = res_row.get('StockPrice')
                    res_tx = res_row.get('TaxPoint')

                    # print('单据时间：' + res_at.strftime('%Y-%m-%d'))
                    # print('单据类型：' + res_st)
                    # print('单据数量：' + str(res_sn))

                    # 计算仓库库龄
                    if need_num > 0:
                        # 需要重新计算仓库库龄的, +
                        if res_st != 'IN7':
                            if not (res_st == 'IN2' and res_as == 0):
                                if need_num > res_sn:
                                    mi_num = res_sn
                                    need_num -= res_sn
                                    if cu_sn - res_sn > 0:
                                        cu_sn -= res_sn
                                    else:
                                        cu_sn = 0
                                else:
                                    mi_num = need_num
                                    need_num = 0
                            # print('本次入后所需库存量：' + str(cu_sn))
                            print('本次入后库龄库存所需量：' + str(need_num))
                            # print('本次入实际消耗库龄库存：' + str(mi_num))

                        # 需要重新计算仓库库龄的, -
                        if res_st != 'OT7':
                            cu_sn += res_sn
                            # print('本次出后所需库存量：' + str(cu_sn))

                        # 计算库龄时间
                        st_delta = cu_date - res_at.date()
                        sto_age = st_delta.days
                        if st_delta.days >= 0 and st_delta.seconds > 0 or st_delta.microseconds > 0:
                            sto_age += 1
                        # 调拨、换货出库、换货入库龄计算
                        # fin_age = sto_age
                        # if res_st in ('IN2', 'IN4', 'OT2', 'OT4'):
                        #     fin_age = None
                        if res_io == 0:
                            if res_st == 'IN2' and res_as == 0:
                                is_tra = 1
                                mi_num = res_sn
                            else:
                                is_tra = 0
                            params_sto = [res_id, res_sno, res_mpi, res_mpn, res_oi, res_on, res_osn, res_st,
                                          res_wi, res_fwi, res_mi, res_mn, res_mt, res_mtc, is_tra, res_mp,
                                          res_tx, res_at, mi_num]
                            sql_sto_ins = 'INSERT INTO tm_stockagecur (Id, StockNo, MainPartId, MainPartName, ' \
                                          'OwnerId, OwnerName, OwnerShotName, StockType, WarehouseId, ' \
                                          'FromWarehouseId, MaterialId, MaterialName, MaterialType, ' \
                                          'MaterialTypeCode, IsTransport, StockPrice, TaxPoint, ' \
                                          'StockTime, StockNum) ' \
                                          'VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, ' \
                                          '%s, %s, %s, %s, %s, %s, %s)'
                            cur.execute(sql_sto_ins, params_sto)
                            con.commit()
                            use_time = datetime.now() - loop_time
                            use_times = use_time.total_seconds()
                            loop_time = datetime.now()
                            print('本轮耗时： ', use_times, ' 秒！')

                # print('仓库库龄剩余需扣库存量：' + str(need_num))
    # 更新全部财务库龄时间
    sql_update_fn = 'UPDATE ods_whcenter.wh_stockagecur x, ' \
                    '(SELECT a.Id,a.MaterialId,a.WarehouseId, ' \
                    'IFNULL((SELECT b.FnStockAge ' \
                    'FROM ods_whcenter.wh_stockagecur b ' \
                    'WHERE b.MaterialId=a.MaterialId ' \
                    'AND b.StockType NOT IN (\'IN2\',\'IN4\',\'IN10\',\'IN13\') ' \
                    'ORDER BY b.FnStockAge DESC ' \
                    'LIMIT 1),(SELECT c.StockAge ' \
                    'FROM ods_whcenter.wh_stockagecur c ' \
                    'WHERE c.MaterialId=a.MaterialId ' \
                    'AND c.StockType NOT IN (\'IN2\',\'IN4\',\'IN10\',\'IN13\') ' \
                    'ORDER BY c.StockAge DESC ' \
                    'LIMIT 1)) AS FnStockAge ' \
                    'FROM ods_whcenter.wh_stockagecur a) AS y ' \
                    'SET x.FnStockAge = y.FnStockAge ' \
                    'WHERE x.Id=y.Id;'
    sql_update_st = 'UPDATE ods_whcenter.wh_stockagecur x, ' \
                    '(SELECT a.Id,a.MaterialId,a.WarehouseId, ' \
                    'IFNULL((SELECT b.FnStockTime ' \
                    'FROM ods_whcenter.wh_stockagecur b ' \
                    'WHERE b.MaterialId=a.MaterialId ' \
                    'AND b.FnStockAge NOT IN (\'IN2\',\'IN4\',\'IN10\',\'IN13\') ' \
                    'ORDER BY b.FnStockTime DESC ' \
                    'LIMIT 1),(SELECT c.StockTime ' \
                    'FROM ods_whcenter.wh_stockagecur c ' \
                    'WHERE c.MaterialId=a.MaterialId ' \
                    'AND c.StockType NOT IN (\'IN2\',\'IN4\',\'IN10\',\'IN13\') ' \
                    'ORDER BY c.StockTime DESC ' \
                    'LIMIT 1)) AS FnStockTime ' \
                    'FROM ods_whcenter.wh_stockagecur a) AS y ' \
                    'SET x.FnStockTime = y.FnStockTime ' \
                    'WHERE x.Id=y.Id;'
    cur.execute(sql_update_fn)
    cur.execute(sql_update_st)
    # 更新财务库龄、在库李玲
    sql_up_fnage = 'UPDATE ods_whcenter.wh_stockagecur ' \
                   'SET FnStockAge = DATEDIFF(FnStockTime,CURRENT_DATE())' \
                   'WHERE 1=1;'
    sql_up_age = 'UPDATE ods_whcenter.wh_stockagecur ' \
                 'SET StockAge = DATEDIFF(StockTime,CURRENT_DATE())' \
                 'WHERE 1=1;'
    cur.execute(sql_up_fnage)
    cur.execute(sql_up_age)
    con.commit()
    total_time = datetime.now() - cu_time
    total_times = total_time.total_seconds()
    print('总计耗时： ', total_times, ' 秒！')
except Exception as e:
    con.rollback()
    print(e)
finally:
    cur.close()
    con.close()
