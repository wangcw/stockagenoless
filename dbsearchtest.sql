SELECT COUNT(1)
FROM wh_stockagedetail;

SELECT *
FROM wh_stockagedetail
WHERE SupplierName IS NULL
AND Id > 1000
ORDER BY Id;

SELECT * FROM wh_stockagedetail WHERE Id=20838;

SELECT COUNT(1)
FROM wh_stockagedetail
WHERE SupplierName IS NULL;

SELECT *
FROM wh_stockagecur;

SELECT COUNT(1)
FROM wh_stockagecur;

select *
from (
SELECT DISTINCT a.WarehouseId, a.MaterialId, a.OwnerId, a.StockPrice, a.TaxPoint
FROM locationcenter.wh_stockagedetail a WHERE EXISTS(SELECT 1 FROM tm_ownerstockinfo b
WHERE b.MaterialId=a.MaterialId AND b.WarehouseId=a.WarehouseId)
AND WarehouseId='WH9999988104'
AND MaterialId='MI0000013238'
AND OwnerId='ON9999999997') as s;

当前处理物料："MI0000013238" ,仓库："WH9999988104",货主："ON9999999997",行号：None

SELECT COUNT(distinct WarehouseId, MaterialId, OwnerId) / 30884
FROM wh_stockagecur;

SELECT SUM(StockNum)
FROM wh_stockagecur
; # 238301
SELECT SUM(StockNum)
FROM tm_ownerstockinfo;
# 234155

truncate table tb_ownerstockdiff2;
INSERT INTO tb_ownerstockdiff2(WarehouseId,MaterialId,OwnerId,StockAgeNum,OwnerNum)

# 货主库存核对
SELECT a.WarehouseId,a.MaterialId,a.OwnerId,a.StockNum,b.StockNum
FROM
(SELECT WarehouseId,MaterialId,OwnerId,SUM(StockNum) AS StockNum
FROM wh_stockagecur
WHERE IsTransport=0
GROUP BY WarehouseId,MaterialId,OwnerId) a,
(SELECT WarehouseId,MaterialId,OwnerId,SUM(RealityNum) AS StockNum
FROM tm_ownerstockinfo
GROUP BY WarehouseId,MaterialId,OwnerId) b
WHERE a.WarehouseId=b.WarehouseId
AND a.MaterialId=b.MaterialId
AND a.OwnerId=b.OwnerId
AND a.OwnerId='ON9999999998'
AND a.StockNum<>b.StockNum
ORDER BY abs(a.StockNum-b.StockNum) DESC;

# 在途库存核对
SELECT a.WarehouseId,a.MaterialId,a.OwnerId,a.StockNum,b.StockNum
FROM
    (SELECT WarehouseId,MaterialId,OwnerId,SUM(StockNum) AS StockNum
     FROM wh_stockagecur
     WHERE IsTransport=1
     GROUP BY WarehouseId,MaterialId,OwnerId) a,
    (SELECT WarehouseId,MaterialId,OwnerId,SUM(StockNum-RealityNum) AS StockNum
     FROM tm_ownerstockinfo
     GROUP BY WarehouseId,MaterialId,OwnerId) b
WHERE a.WarehouseId=b.WarehouseId
  AND a.MaterialId=b.MaterialId
  AND a.OwnerId=b.OwnerId
  AND a.StockNum<>b.StockNum
ORDER BY abs(a.StockNum-b.StockNum) DESC;

# 在库
INSERT INTO tm_noownerstock(WarehosueId, MaterialId, OwnerId)
SELECT WarehouseId, MaterialId, OwnerId
FROM     (SELECT WarehouseId,MaterialId,OwnerId,SUM(RealityNum) AS StockNum
          FROM tm_ownerstockinfo
          WHERE RealityNum <> 0
          GROUP BY WarehouseId,MaterialId,OwnerId) b
    WHERE NOT EXISTS(SELECT 1
    FROM
    (SELECT WarehouseId,MaterialId,OwnerId,SUM(StockNum) AS StockNum
     FROM wh_stockagecur
     WHERE IsTransport=0
     GROUP BY WarehouseId,MaterialId,OwnerId) a
WHERE a.WarehouseId=b.WarehouseId
  AND a.MaterialId=b.MaterialId
  AND a.OwnerId=b.OwnerId)
ORDER BY 1,2;

SELECT CONCAT('(''',WarehosueId,''', ''', MaterialId,''', ''', OwnerId, ''', ''',
CASE OwnerId WHEN 'ON9999999990' THEN '青岛车智品信息科技有限公司'
  WHEN 'ON9999999997' THEN '青岛中瑞汽车服务有限公司'
  WHEN 'ON9999999995' THEN '青岛中瑞正联科技有限公司' END,''', ''',
CASE OwnerId WHEN 'ON9999999990' THEN '车智品'
  WHEN 'ON9999999997' THEN '中瑞汽车'
  WHEN 'ON9999999995' THEN '中瑞正联' END,'''),')
FROM tm_noownerstock;

SELECT *
FROM wh_stockagedetail a,
     tm_noownerstock c
WHERE a.WarehouseId=c.WarehosueId
    AND a.MaterialId=c.MaterialId
    AND NOT EXISTS(SELECT 1
                 FROM wh_stockagecur b
                 WHERE b.Id=a.Id);

UPDATE wh_stockagedetail a,
    tm_noownerstock c
SET a.OwnerId=c.OwnerId,
    a.OwnerName=CASE c.OwnerId WHEN 'ON9999999990' THEN '青岛车智品信息科技有限公司'
        WHEN 'ON9999999997' THEN '青岛中瑞汽车服务有限公司'
        WHEN 'ON9999999998' THEN '青岛中瑞车联电子科技有限公司'
        WHEN 'ON9999999995' THEN '青岛中瑞正联科技有限公司' END,
    a.OwnerShortName=CASE c.OwnerId WHEN 'ON9999999990' THEN '车智品'
                                    WHEN 'ON9999999997' THEN '中瑞汽车'
                                    WHEN 'ON9999999998' THEN '中瑞车联'
                                    WHEN 'ON9999999995' THEN '中瑞正联' END
WHERE a.WarehouseId=c.WarehosueId
  AND a.MaterialId=c.MaterialId
  AND NOT EXISTS(SELECT 1
                 FROM wh_stockagecur b
                 WHERE b.Id=a.Id);

SELECT DISTINCT OwnerId,OwnerName,OwnerShortName
FROM wh_stockagedetail s
WHERE OwnerId IN ('ON9999999990','ON9999999995','ON9999999997');

SELECT *
FROM wh_stockagedetail s
WHERE EXISTS(
SELECT 1
FROM (SELECT *
FROM     (SELECT WarehouseId,MaterialId,OwnerId,SUM(RealityNum) AS StockNum
          FROM tm_ownerstockinfo
          WHERE RealityNum <> 0
          GROUP BY WarehouseId,MaterialId,OwnerId) b
WHERE NOT EXISTS(SELECT 1
                 FROM
                     (SELECT WarehouseId,MaterialId,OwnerId,SUM(StockNum) AS StockNum
                      FROM wh_stockagecur
                      WHERE IsTransport=0
                      GROUP BY WarehouseId,MaterialId,OwnerId) a
                 WHERE a.WarehouseId=b.WarehouseId
                   AND a.MaterialId=b.MaterialId
                   AND a.OwnerId=b.OwnerId) ) SS
    where ss.WarehouseId=s.WarehouseId
    and ss.MaterialId=s.MaterialId);


SELECT *
FROM wh_stockagecur
WHERE WarehouseId='WH9999993069'
AND MaterialId='MI0000008722'
AND OwnerId='ON9999999997';

SELECT sum(StockNum)
FROM wh_stockagecur
WHERE IsTransport=0;
#218268

SELECT sum(RealityNum)
FROM tm_ownerstockinfo;
#218268

UPDATE wh_stockagedetail a,
    wh_stockagecur b
SET b.SupplierName=a.SupplierName,
    b.StockPrice=a.StockPrice,
    b.TaxPoint=a.TaxPoint
WHERE a.Id=b.Id;

SELECT COUNT(1)
FROM wh_stockagedetail;


SELECT *
FROM wh_stockagedetail
WHERE WarehouseId='WH9999993069'
  AND MaterialId='MI0000008722'
  AND OwnerId='ON9999999997';



SELECT *
FROM tm_ownerstockinfo;

SELECT *
FROM wh_stockagecur
WHERE IsTransport=1;

SELECT * FRom tb_ownerstockdiff #where StockAgeNum>OwnerNum;
where WarehouseId='WH9999995727'
and MaterialId='MI0000008713';

select *
from tm_ownerstockinfo
where WarehouseId='WH9999995727'
  and MaterialId='MI0000008713';

select *
from wh_stockagedetail a
where WarehouseId='WH9999995727'
  and MaterialId='MI0000008713'
and ownerid is null;

select *
from wh_stockagedetail a
where WarehouseId='WH9999999693'
  and MaterialId='MI0000013859';

select *
from tm_ownerstockinfo a
where WarehouseId='WH9999999693'
  and MaterialId='MI0000013859';

select *
from wh_stockagedetail a
where ownerid is null
and exists (select 1 from tb_ownerstockdiff b where b.MaterialId=a.MaterialId and b.WarehouseId = a.WarehouseId);

select * from wh_stockagedetail where id=158657;

select * from wh_stockagedetail where id=70;

UPDATE wh_stockagedetail
SET OwnerId='ON9999999997',
    OwnerName='青岛中瑞汽车服务有限公司',
    OwnerShortName='中瑞汽车'
WHERE WarehouseId='WH9999995727'
  and MaterialId='MI0000008713'
  and ownerid is null;

UPDATE wh_stockagedetail a
SET a.OwnerId='ON9999999998',
    a.OwnerName='青岛中瑞车联电子科技有限公司',
    a.OwnerShortName='中瑞车联'
WHERE a.OwnerId is null
  and exists (select 1 from tb_ownerstockdiff b where b.MaterialId=a.MaterialId and b.WarehouseId = a.WarehouseId);


select * from wh_stockagedetail
where OwnerId='ON9999999997';




SELECT COUNT(distinct WarehouseId, MaterialId, OwnerId)
FROM wh_stockagedetail;

UPDATE wh_stockagecur a,wh_stockagedetail b
set a.SupplierName=b.SupplierName
WHERE a.Id=b.Id;

UPDATE wh_stockagecur a,wh_stockagedetail b
SET a.FromWarehouseName=b.FromWarehouseName
WHERE a.Id=b.Id;


SELECT a.MainPartName AS '销售平台',
    a.OwnerName AS '货主名称',
    a.OwnerShortName AS '货主简称',
    a.SupplierName AS '供应商全称',
    a.StockNo AS '入库单号',
    IF(b.LevelCode=2,c.Name,b.Name) AS '所在一级仓',
    b.Name AS '所在二级仓',
    IFNULL(IF(d.LevelCode=2,e.Name,d.Name),a.FromWarehouseName) AS '最后所出二级仓库',
    IFNULL(d.Name,a.FromWarehouseName) AS '最后所出一级仓库',
    CASE b.Mode
      WHEN 0 THEN '普通仓'
      WHEN 1 THEN '优工仓'
      WHEN 2 THEN '门店仓'
      WHEN 3 THEN '数据仓'
      WHEN 4 THEN '租赁仓'
      WHEN 5 THEN '自营仓'
      WHEN 6 THEN '客户仓'
    END AS '经营模式',
       CASE a.StockType
           WHEN 'IN0' THEN '采购入库'
           WHEN 'IN1' THEN '迁移入库'
           WHEN 'IN2' THEN '调拨入库'
           WHEN 'IN3' THEN '退货入库'
           WHEN 'IN4' THEN '更换入库'
           WHEN 'IN5' THEN '组装入库'
           WHEN 'IN6' THEN '拆装入库'
           WHEN 'IN7' THEN '修改入库'
           WHEN 'IN8' THEN '采购换货入库'
           WHEN 'IN9' THEN '生产入库'
           WHEN 'IN10' THEN '货主交易入库'
           WHEN 'IN11' THEN '代管入库'
           WHEN 'IN12' THEN '推广入库'
           WHEN 'IN13' THEN '货主变更入库'
           WHEN 'IN14' THEN '迁移更换入库'
           END AS 出入库类型,
    a.MaterialType AS '物料类型',
    a.MaterialName AS '物料名称',
    a.StockNum AS '库存数量',
    a.StockPrice AS '采购单价',
    a.TaxPoint AS '采购税率',
    a.StockAge AS '库龄',
    a.StockTime AS '最近一次入此仓库时间',
    a.FnStockAge AS '财务管理库龄',
    a.FnStockTime AS '首次进入系统时间',
    CASE a.IsTransport WHEN 1 THEN '是' ELSE '否' END AS 是否在途,
    CURRENT_DATE() AS '报表日期'
FROM wh_stockagecur a
LEFT JOIN tb_warehouse b
  ON b.Id=a.WarehouseId
  AND b.Deleted=0
LEFT JOIN tb_warehouse c
  ON c.Id=b.ParentId
  AND c.Deleted=0
LEFT JOIN tb_warehouse d
  ON d.Id=a.FromWarehouseId
  AND d.Deleted=0
LEFT JOIN tb_warehouse e
  ON e.Id=d.ParentId
  AND e.Deleted=0;

SELECT b.Name AS 仓库名称,c.Name AS 货主名称,c.ShortName AS 货主简称,
       (SELECT MaterialName FROM tb_instockdetail d WHERE d.MaterialId=a.MaterialId AND d.Deleted=0 LIMIT 1) AS 物料名称,
       a.StockNum AS 货主库存,a.RealityNum AS `在库库存(去掉在途)`
FROM tm_ownerstockinfo a
LEFT JOIN tb_warehouse b
  ON b.Id=a.WarehouseId
LEFT JOIN tb_ownerinfo c
  ON c.Id=a.OwnerId;

SELECT *
FROM tb_ownerstockdiff;

SELECT *
FROM wh_stockagecur
WHERE FnStockAge IS NULL;

SELECT * FROM
    (SELECT a.Id,a.MaterialId,a.WarehouseId,
            IFNULL((SELECT b.FnStockTime
                    FROM locationcenter.wh_stockagecur b
                    WHERE b.MaterialId=a.MaterialId
                      AND b.FnStockAge NOT IN ('IN2','IN4','IN10','IN13')
                    ORDER BY b.FnStockTime DESC
                    LIMIT 1),(SELECT c.StockTime
                              FROM locationcenter.wh_stockagecur c
                              WHERE c.MaterialId=a.MaterialId
                                AND c.StockType NOT IN ('IN2','IN4','IN10','IN13')
                              ORDER BY c.StockTime DESC
                              LIMIT 1)) AS FnStockTime
     FROM locationcenter.wh_stockagecur a) AS y
WHERE y.FnStockTime is null;

SELECT *
FROM locationcenter.wh_stockagecur b
WHERE b.MaterialId='MI0000013859'
  # AND b.FnStockAge NOT IN ('IN2','IN4','IN10','IN13')
ORDER BY b.FnStockTime DESC;

UPDATE locationcenter.wh_stockagecur x,
(SELECT b.Id, b.MaterialId, b.AuditTime AS FnStockTime
FROM locationcenter.wh_stockagedetail b
WHERE b.StockType NOT IN ('IN2','IN4','IN10','IN13')
ORDER BY b.AuditTime DESC
LIMIT 1) AS y
SET x.FnStockTime = y.FnStockTime
WHERE x.Id=y.Id;

SELECT * FROM wh_stockagecur WHERE FnStockTime IS NULL;

UPDATE wh_stockagecur x,
(SELECT a.MaterialId,a.AuditTime,
ROW_NUMBER() OVER(PARTITION BY a.MaterialId ORDER BY a.AuditTime DESC) AS num
FROM wh_stockagedetail a
WHERE a.StockType NOT IN ('IN2','IN4','IN10','IN13')) y
SET x.FnStockTime=y.AuditTime
WHERE x.MaterialId=y.MaterialId
AND y.num=1;

insert into wh_stockagecur_bak
select * from wh_stockagecur;

SELECT * FROM wh_stockagecur where stockno='IN20230615154714231C84C';

SELECT *
FROM tm_ownerstockinfo
WHERE WarehouseId='WH9999999998'
AND MaterialId='MI0000002514'
AND OwnerId='ON9999999998';


SELECT *
FROM wh_stockagedetail
WHERE  WarehouseId='WH9999999998'
  AND MaterialId='MI0000002514'
  AND OwnerId IS NULL;

SELECT *
FROM wh_stockagecur
WHERE WarehouseId='WH9999999998'
  AND MaterialId='MI0000002514'
  AND OwnerId='ON9999999998';

SELECT *
FROM wh_stockagedetail
WHERE WarehouseId='WH9999999998'
  AND MaterialId='MI0000002514'
  AND OwnerId='ON9999999998';
ORDER BY AuditTime desc
and StockType like 'IN%';

SELECT *
FROM wh_stockagedetail
WHERE OwnerId IS not NULL
ORDER BY AuditTime DESC;