#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2023/8/3 07:42

import threading
import pymysql.cursors
from dbutils.pooled_db import PooledDB
import configparser
from datetime import datetime

config = configparser.ConfigParser()
config.read('db.conf')

my_host = config.get('whpro', 'host')
my_database = config.get('whpro', 'database')
my_user = config.get('whpro', 'user')
my_password = config.get('whpro', 'password')

try:
    mypool = PooledDB(
        creator=pymysql,
        maxconnections=10,
        mincached=2,
        maxcached=5,
        maxshared=3,
        blocking=True,
        maxusage=None,
        setsession=[],
        ping=1,
        host=my_host,
        port=3306,
        user=my_user,
        password=my_password,
        charset="utf8",
        db=my_database,
        cursorclass=pymysql.cursors.DictCursor
    )
except Exception as e:
    print('mysql连接错误原因：', e)

# 开启多线程数量
thread_count = 10
# 每批次迁移大小
batch_size = 1000
# 开始行
offset = 0


def distribute_data():
    global offset
    offset += batch_size
    return offset


# 写入基础表
with mypool.connection() as con:
    cur = con.cursor()
    try:
        # 物料流水表
        table_start = datetime.now()
        sql_material_dro = 'DROP TABLE IF EXISTS wh_stockagedetail;'
        sql_material_cre = 'CREATE TABLE wh_stockagedetail( ' \
                           'Id bigint auto_increment primary key , ' \
                           'StockId            varchar(100), ' \
                           'StockNo            varchar(100), ' \
                           'FromWarehouseId    varchar(100), ' \
                           'FromWarehouseName  varchar(100), ' \
                           'SupplierName       varchar(100), ' \
                           'MainPartId         varchar(100), ' \
                           'MainPartName       varchar(100), ' \
                           'WarehouseId        varchar(100), ' \
                           'MaterialId         varchar(100), ' \
                           'MaterialName       varchar(100), ' \
                           'MaterialType       varchar(100), ' \
                           'MaterialTypeCode   varchar(100), ' \
                           'InOrOut            int default 0, ' \
                           'AuditState         int default 0, ' \
                           'StockType          varchar(100), ' \
                           'OwnerId            varchar(100), ' \
                           'OwnerName          varchar(100), ' \
                           'OwnerShortName     varchar(100), ' \
                           'AuditTime          datetime, ' \
                           'StockNum           int, ' \
                           'StockPrice         decimal(18, 4), ' \
                           'TaxPoint           int , ' \
                           'KEY `1` (StockId), ' \
                           'KEY `2` (FromWarehouseId), ' \
                           'KEY `3` (SupplierName), ' \
                           'KEY `4` (MaterialId), ' \
                           'KEY `5` (InOrOut), ' \
                           'KEY `6` (WarehouseId), ' \
                           'KEY `7` (StockType), ' \
                           'KEY `8` (StockNum) ' \
                           ');'
        # 获取货主库存
        sql_stock_dro = 'DROP TABLE IF EXISTS tm_ownerstockinfo;'
        sql_stock_cre = 'CREATE TABLE tm_ownerstockinfo ( ' \
                        'Id bigint NOT NULL AUTO_INCREMENT COMMENT \'自增主键\', ' \
                        'WarehouseId varchar(100) COMMENT \'仓库Id(tb_warehouse.Id)\', ' \
                        'MaterialId varchar(100) COMMENT \'物料Id\', ' \
                        'OwnerId varchar(100) COMMENT \'货主Id\', ' \
                        'StockNum int COMMENT \'在库数量\', ' \
                        'RealityNum int COMMENT \'可用库存\', ' \
                        'primary key (id), ' \
                        'key `1` (WarehouseId), ' \
                        'key `2` (MaterialId), ' \
                        'key `3` (OwnerId) ' \
                        ');'
        sql_stock_ins = 'INSERT INTO tm_ownerstockinfo(Id, WarehouseId, MaterialId, ' \
                        'OwnerId, StockNum, RealityNum) SELECT Id, WarehouseId, MaterialId, OwnerId, StockNum, ' \
                        'RealityNum FROM tb_ownerstockinfo ' \
                        'WHERE IsCodeSingle = 0 AND StockNum <> 0 AND Deleted = 0;'

        # 写入物料流水数据
        sql_instock_ins = 'INSERT INTO wh_stockagedetail (StockId,StockNo,FromWarehouseId,' \
                          'FromWarehouseName,MainPartId,MainpartName,WarehouseId,MaterialId,MaterialName,MaterialType, ' \
                          'MaterialTypeCode,InOrOut,AuditState,StockType,OwnerId,OwnerName,OwnerShortName, ' \
                          'AuditTime,StockNum,StockPrice,TaxPoint)SELECT a.Id,a.InStockNo,a.FromWarehouseId, ' \
                          'a.FromWarehouseName,a.MainPartId,e.Name AS MainpartName,a.WarehouseId, ' \
                          'b.MaterialId,b.MaterialName,b.MaterialType,b.MaterialTypeCode,0 AS InOrOut, ' \
                          'a.AuditState,a.InStockType AS StockType,IFNULL(c.OwnerId,b.OwnerId) AS OwnerId, ' \
                          'f.Name AS OwnerName,f.ShortName AS OwnerShortName,a.AuditTime, ' \
                          'COALESCE(d.StockNum,c.InStockNum,b.InStockedNum) AS StockNum,d.StockPrice, ' \
                          'd.TaxPoint FROM tb_instockinfo a ' \
                          'INNER JOIN tb_instockdetail b ON b.InStockId=a.Id ' \
                          'AND b.Deleted=0 AND b.IsCodeSingle=0 ' \
                          'LEFT JOIN tb_instockdetailextra c ON c.InStockDetailId=b.Id ' \
                          'AND c.Deleted=0 LEFT JOIN tb_purchasedetailextra d ' \
                          'ON d.StockDetailId=b.Id AND d.Deleted=0 LEFT JOIN tb_mainpartinfo e ' \
                          'ON e.ProductCenterId=a.MainPartId AND e.Deleted=0 ' \
                          'LEFT JOIN tb_ownerinfo f ON f.Id=IFNULL(c.OwnerId,b.OwnerId) ' \
                          'AND f.Deleted=0 WHERE a.AuditState IN (0,2) AND a.Deleted=0 AND a.AuditTime <= %s UNION ' \
                          'SELECT ah.Id,ah.InStockNo,ah.FromWarehouseId,ah.FromWarehouseName,ah.MainPartId, ' \
                          'eh.Name AS MainpartName,ah.WarehouseId,bh.MaterialId,bh.MaterialName, ' \
                          'bh.MaterialType,bh.MaterialTypeCode,0 AS InOrOut,ah.AuditState,ah.InStockType ' \
                          'AS StockType,IFNULL(ch.OwnerId,bh.OwnerId) AS OwnerId,fh.Name AS OwnerName, ' \
                          'fh.ShortName AS OwnerShortName,ah.AuditTime,COALESCE(dh.StockNum,ch.InStockNum, ' \
                          'bh.InStockedNum) AS StockNum,dh.StockPrice,dh.TaxPoint ' \
                          'FROM tb_instockinfohis ah ' \
                          'INNER JOIN tb_instockdetailhis bh ON bh.InStockId=ah.Id ' \
                          'AND bh.Deleted=0 AND bh.IsCodeSingle=0  ' \
                          'LEFT JOIN tb_instockdetailextra ch ' \
                          'ON ch.InStockDetailId=bh.Id AND ch.Deleted=0 ' \
                          'LEFT JOIN tb_purchasedetailextra dh ON dh.StockDetailId=bh.Id ' \
                          'AND dh.Deleted=0 LEFT JOIN tb_mainpartinfo eh ' \
                          'ON eh.ProductCenterId=ah.MainPartId AND eh.Deleted=0 ' \
                          'LEFT JOIN tb_ownerinfo fh ON fh.Id=IFNULL(ch.OwnerId,bh.OwnerId) ' \
                          'AND fh.Deleted=0 WHERE ah.AuditState = 2 AND ah.Deleted=0 ' \
                          'AND ah.AuditTime <= %s ORDER BY AuditTime DESC;'
        sql_outstock_ins = 'INSERT INTO wh_stockagedetail (StockId,StockNo,MainPartId,MainpartName, ' \
                           'WarehouseId,MaterialId,MaterialName,MaterialType,MaterialTypeCode,InOrOut,AuditState, ' \
                           'StockType,OwnerId,OwnerName,OwnerShortName,AuditTime,StockNum,StockPrice,TaxPoint) ' \
                           'SELECT ao.Id,ao.OutStockNo,ao.MainPartId,eo.Name AS MainpartName,ao.WarehouseId, ' \
                           'bo.MaterialId,bo.MaterialName,bo.MaterialType,bo.MaterialTypeCode,1 AS InOrOut, ' \
                           'ao.AuditState,ao.OutStockType AS StockType,IFNULL(co.OwnerId,bo.OwnerId) ' \
                           'AS OwnerId,fo.Name AS OwnerName,fo.ShortName AS OwnerShortName,ao.AuditTime,' \
                           'IFNULL(co.OutStockNum,bo.OutStockedNum)AS StockNum,NULL AS StockPrice,' \
                           'NULL AS TaxPoint FROM tb_outstockinfo ao ' \
                           'INNER JOIN tb_outstockdetail bo ON bo.OutStockId=ao.Id AND bo.Deleted=0 ' \
                           'AND bo.IsCodeSingle=0 LEFT JOIN tb_outstockdetailextra co ' \
                           'ON co.OutStockDetailId=bo.Id AND co.Deleted=0 ' \
                           'LEFT JOIN tb_mainpartinfo eo ' \
                           'ON eo.ProductCenterId=ao.MainPartId AND eo.Deleted=0 ' \
                           'LEFT JOIN tb_ownerinfo fo ON fo.Id = IFNULL(co.OwnerId,bo.OwnerId) ' \
                           'AND fo.Deleted=0 WHERE ao.AuditState IN (3,4) AND ao.Deleted=0 ' \
                           'AND ao.AuditTime <= %s UNION ' \
                           'SELECT aoh.Id,aoh.OutStockNo,aoh.MainPartId,eoh.Name AS MainpartName,aoh.WarehouseId, ' \
                           'boh.MaterialId,boh.MaterialName,boh.MaterialType,boh.MaterialTypeCode,1 AS InOrOut, ' \
                           'aoh.AuditState,aoh.OutStockType AS StockType,IFNULL(coh.OwnerId,boh.OwnerId) AS OwnerId,' \
                           'foh.Name AS OwnerName,foh.ShortName AS OwnerShortName,aoh.AuditTime,' \
                           'IFNULL(coh.OutStockNum,boh.OutStockedNum) AS StockNum, NULL AS StockPrice,NULL AS TaxPoint ' \
                           'FROM tb_outstockinfohis aoh ' \
                           'INNER JOIN tb_outstockdetailhis boh ON boh.OutStockId=aoh.Id ' \
                           'AND boh.Deleted=0 AND boh.IsCodeSingle=0 ' \
                           'LEFT JOIN tb_outstockdetailextra coh ON coh.OutStockDetailId=boh.Id ' \
                           'AND coh.Deleted=0 LEFT JOIN tb_mainpartinfo eoh ON eoh.ProductCenterId= ' \
                           'aoh.MainPartId AND eoh.Deleted=0 LEFT JOIN tb_ownerinfo foh ' \
                           'ON foh.Id=IFNULL(coh.OwnerId,boh.OwnerId) AND foh.Deleted=0 WHERE aoh.AuditState = 4 ' \
                           'AND aoh.Deleted=0  AND aoh.AuditTime <= %s ORDER BY AuditTime DESC;'

        # 新建缺失货主问题排查临时表
        sql_diff_dro = 'DROP TABLE IF EXISTS tb_ownerstockdiff;'
        sql_diff_cre = 'CREATE TABLE tb_ownerstockdiff ( ' \
                       'Id int auto_increment primary key,' \
                       'WarehouseId varchar(100) , ' \
                       'MaterialId varchar(100) , ' \
                       'OwnerId varchar(100) , ' \
                       'OwnerName varchar(100) , ' \
                       'OwnerShortName varchar(100) , ' \
                       'StockAgeNum int , ' \
                       'OwnerNum int );'

        # 货主更新
        sql_diff_ins1 = 'INSERT INTO tb_ownerstockdiff(WarehouseId, MaterialId, OwnerId, OwnerName, OwnerShortName) ' \
                        'SELECT a.WarehouseId,a.MaterialId,a.OwnerId, \'青岛中瑞车联电子科技有限公司\', \'中瑞车联\' ' \
                        'FROM (SELECT WarehouseId,MaterialId,OwnerId,SUM(StockNum) AS StockNum ' \
                        'FROM wh_stockagecur WHERE IsTransport=0 GROUP BY WarehouseId,MaterialId,OwnerId) a, ' \
                        '(SELECT WarehouseId,MaterialId,OwnerId,SUM(RealityNum) AS StockNum FROM tm_ownerstockinfo ' \
                        'GROUP BY WarehouseId,MaterialId,OwnerId) b WHERE a.WarehouseId=b.WarehouseId ' \
                        'AND a.MaterialId=b.MaterialId AND a.OwnerId=b.OwnerId AND a.StockNum<>b.StockNum ' \
                        'AND a.OwnerId = \'ON9999999998\' ORDER BY ABS(a.StockNum-b.StockNum) DESC;'

        sql_diff_ins2 = 'INSERT INTO tb_ownerstockdiff(WarehouseId,MaterialId,OwnerId,OwnerName,OwnerShortName) ' \
                        'VALUES  ' \
                        '(\'WH9999991638\', \'MI0000008353\', \'ON9999999990\', \'青岛车智品信息科技有限公司\', \'车智品\'), ' \
                        '(\'WH9999992240\', \'MI0000008713\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999992558\', \'MI0000008353\', \'ON9999999990\', \'青岛车智品信息科技有限公司\', \'车智品\'), ' \
                        '(\'WH9999992581\', \'MI0000008353\', \'ON9999999990\', \'青岛车智品信息科技有限公司\', \'车智品\'), ' \
                        '(\'WH9999992584\', \'MI0000008353\', \'ON9999999990\', \'青岛车智品信息科技有限公司\', \'车智品\'), ' \
                        '(\'WH9999992585\', \'MI0000008353\', \'ON9999999990\', \'青岛车智品信息科技有限公司\', \'车智品\'), ' \
                        '(\'WH9999992827\', \'MI0000007868\', \'ON9999999990\', \'青岛车智品信息科技有限公司\', \'车智品\'), ' \
                        '(\'WH9999992827\', \'MI0000007904\', \'ON9999999990\', \'青岛车智品信息科技有限公司\', \'车智品\'), ' \
                        '(\'WH9999992885\', \'MI0000008713\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999992885\', \'MI0000009148\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999993069\', \'MI0000008713\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999993069\', \'MI0000008718\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999993069\', \'MI0000008719\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999993069\', \'MI0000008722\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999994276\', \'MI0000008713\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999994276\', \'MI0000008718\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999994276\', \'MI0000008719\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999994276\', \'MI0000008722\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999994276\', \'MI0000008766\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999994611\', \'MI0000008718\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999994611\', \'MI0000008825\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999994611\', \'MI0000008856\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999995223\', \'MI0000008766\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999995223\', \'MI0000008767\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999995244\', \'MI0000008856\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999995727\', \'MI0000008718\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999995727\', \'MI0000008719\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999995727\', \'MI0000008722\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999995791\', \'MI0000008856\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999996955\', \'MI0000009754\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999997080\', \'MI0000008713\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999997561\', \'MI0000008741\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999998579\', \'MI0000008713\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999998579\', \'MI0000008766\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999998579\', \'MI0000008771\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999999045\', \'MI0000008713\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999999362\', \'MI0000009744\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999999401\', \'MI0000008713\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999999401\', \'MI0000008766\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999999401\', \'MI0000008825\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999999505\', \'MI0000008713\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999999505\', \'MI0000008766\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999999505\', \'MI0000008767\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999999505\', \'MI0000008865\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999999862\', \'MI0000008856\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\'), ' \
                        '(\'WH9999999998\', \'MI0000009580\', \'ON9999999995\', \'青岛中瑞正联科技有限公司\', \'中瑞正联\'), ' \
                        '(\'WH9999999998\', \'MI0000009581\', \'ON9999999995\', \'青岛中瑞正联科技有限公司\', \'中瑞正联\'), ' \
                        '(\'WH9999999998\', \'MI0000009582\', \'ON9999999995\', \'青岛中瑞正联科技有限公司\', \'中瑞正联\'), ' \
                        '(\'WH9999999998\', \'MI0000009583\', \'ON9999999995\', \'青岛中瑞正联科技有限公司\', \'中瑞正联\'), ' \
                        '(\'WH9999999998\', \'MI0000009584\', \'ON9999999995\', \'青岛中瑞正联科技有限公司\', \'中瑞正联\'), ' \
                        '(\'WH9999999998\', \'MI0000009585\', \'ON9999999995\', \'青岛中瑞正联科技有限公司\', \'中瑞正联\'), ' \
                        '(\'WH9999999998\', \'MI0000009593\', \'ON9999999995\', \'青岛中瑞正联科技有限公司\', \'中瑞正联\'), ' \
                        '(\'WH9999999998\', \'MI0000009594\', \'ON9999999995\', \'青岛中瑞正联科技有限公司\', \'中瑞正联\'), ' \
                        '(\'WH9999995727\', \'MI0000008713\', \'ON9999999997\', \'青岛中瑞汽车服务有限公司\', \'中瑞汽车\')'

        # 缺失货主更新
        sql_diff_up = 'UPDATE wh_stockagedetail a , tb_ownerstockdiff b ' \
                      'SET a.OwnerId=b.OwnerId,a.OwnerName=b.OwnerName,a.OwnerShortName=b.OwnerShortName ' \
                      'WHERE a.WarehouseId=b.WarehouseId ' \
                      'AND a.MaterialId=b.MaterialId ' \
                      'AND a.OwnerId IS NULL;'

        cur.execute(sql_material_dro)
        cur.execute(sql_material_cre)
        cur.execute(sql_stock_dro)
        cur.execute(sql_stock_cre)
        cur.execute(sql_stock_ins)
        sotck_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        cur.execute(sql_instock_ins, [sotck_time, sotck_time])
        cur.execute(sql_outstock_ins, [sotck_time, sotck_time])
        cur.execute(sql_diff_dro)
        cur.execute(sql_diff_cre)
        cur.execute(sql_diff_ins1)
        cur.execute(sql_diff_ins2)
        cur.execute(sql_diff_up)

    except Exception as e:
        print(e)
    finally:
        cur.close()
        con.close()
    table_time = datetime.now()
    print('基础数据写入完成，耗时 {} 秒！'.format((table_time - table_start).total_seconds()))


# 数据迁移处理
def handle_batch(thread_name):
    prs = 'default'
    while len(prs) > 0:
        batch_start = datetime.now()
        offsets = distribute_data()
        with mypool.connection() as con:
            # 获取数据
            sql_no_st = 'SELECT Id, MaterialId, AuditTime, StockPrice FROM wh_stockagedetail ' \
                        'ORDER BY Id LIMIT {}, {};'
            # 查找更新
            sql_find_st = 'SELECT d.Name AS SupplierName, ' \
                          'IFNULL(c.StockPrice,b.StockPrice) AS StockPrice, ' \
                          'IFNULL(c.TaxPoint,b.TaxPoint) AS TaxPoint ' \
                          'FROM tb_purchaseinfo a ' \
                          'INNER JOIN tb_purchasedetail b ' \
                          'ON b.PurchaseId=a.Id ' \
                          'AND b.MaterialId=%s ' \
                          'AND b.Deleted=0 ' \
                          'LEFT JOIN tb_purchasedetailextra c ' \
                          'ON c.PurchaseDetailId=b.Id ' \
                          'AND c.Deleted=0 ' \
                          'LEFT JOIN tb_supplier d ' \
                          'ON d.Id=b.SupplierId ' \
                          'AND d.Deleted=0 ' \
                          'WHERE a.Deleted=0 AND a.PurchaseType IN (\'CG0\',\'CG3\') ' \
                          'AND a.AuditTime <= %s ' \
                          'AND a.AuditState IN (3,4,8) ' \
                          'ORDER BY a.AuditTime DESC LIMIT 1;'
            # 更新数据
            sql_up_st = 'UPDATE wh_stockagedetail SET SupplierName = %s,StockPrice = %s,TaxPoint = %s WHERE Id = %s;'
            sql_no_st = sql_no_st.format(int(offsets), int(batch_size))
            try:
                cur = con.cursor()
                cur.execute(sql_no_st)
                prs = cur.fetchall()
                for pr in prs:
                    pr_id = pr.get('Id')
                    pr_materialid = pr.get('MaterialId')
                    pr_audittime = pr.get('AuditTime')

                    cur.execute(sql_find_st, [pr_materialid, pr_audittime])
                    pu = cur.fetchall()
                    if pu:
                        pu_sn = pu[0].get('SupplierName')
                        pu_sp = pu[0].get('StockPrice')
                        pu_tx = pu[0].get('TaxPoint')
                        pr_stockprice = pu_sp
                        pr_taxpoint = pu_tx
                        cur.execute(sql_up_st, [pu_sn, pr_stockprice, pr_taxpoint, pr_id])
                        con.commit()
            except Exception as e:
                print(e)
            finally:
                cur.close()
                con.close()
        batch_time = datetime.now()
        print(f'线程 {thread_name}: 迁移进度: {offset} 条，耗时 {(batch_time - batch_start).total_seconds()} 秒！')


# 多线程开启
threads = []
for i in range(thread_count):
    print(f'线程 {i} 已开启!')
    thread = threading.Thread(target=handle_batch, args=(i,))
    thread.start()
    threads.append(thread)

for thread in threads:
    thread.join()
