#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw
# @generate at 2023/10/8 13:04

import mysql.connector
import configparser
from datetime import datetime

config = configparser.ConfigParser()
config.read("db.conf")

my_host = config.get("test", "host")
my_database = config.get("test", "database")
my_user = config.get("test", "user")
my_password = config.get("test", "password")

con = mysql.connector.connect(
    host=my_host, user=my_user, password=my_password, database=my_database
)
cu_time = datetime.now()
cu_date = cu_time.date()
loop_time = datetime.now()

try:
    cur = con.cursor(dictionary=True)

    # 写入每月新明细数据
    # 写入物料流水数据
    sql_instock_ins = (
        "INSERT INTO wh_stockagedetail (StockId,DetailId,StockNo,FromWarehouseId,"
        "FromWarehouseName,MainPartId,MainpartName,WarehouseId,MaterialId,MaterialName,MaterialType, "
        "MaterialTypeCode,InOrOut,AuditState,StockType,OwnerId,OwnerName,OwnerShortName, "
        "AuditTime,StockNum,StockPrice,TaxPoint, CheckDate) SELECT a.Id,b.Id,a.InStockNo,a.FromWarehouseId, "
        "a.FromWarehouseName,a.MainPartId,e.Name AS MainpartName,a.WarehouseId, "
        "b.MaterialId,b.MaterialName,b.MaterialType,b.MaterialTypeCode,0 AS InOrOut, "
        "a.AuditState,a.InStockType AS StockType,IFNULL(c.OwnerId,b.OwnerId) AS OwnerId, "
        "f.Name AS OwnerName,f.ShortName AS OwnerShortName,a.AuditTime, "
        "COALESCE(d.StockNum,c.InStockNum,b.InStockedNum) AS StockNum,d.StockPrice, "
        "d.TaxPoint,CURDATE() FROM tb_instockinfo a "
        "INNER JOIN tb_instockdetail b ON b.InStockId=a.Id "
        "AND b.Deleted=0 AND b.IsCodeSingle=0 "
        "LEFT JOIN tb_instockdetailextra c ON c.InStockDetailId=b.Id "
        "AND c.Deleted=0 LEFT JOIN tb_purchasedetailextra d "
        "ON d.StockDetailId=b.Id AND d.Deleted=0 LEFT JOIN tb_mainpartinfo e "
        "ON e.ProductCenterId=a.MainPartId AND e.Deleted=0 "
        "LEFT JOIN tb_ownerinfo f ON f.Id=IFNULL(c.OwnerId,b.OwnerId) "
        "AND f.Deleted=0 WHERE a.AuditState IN (0,2) AND a.Deleted=0 "
        "AND a.AuditTime < CONCAT(DATE_FORMAT(CURDATE(), '%Y-%m'),'-01') "
        # "AND a.AuditTime >= CONCAT(DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -1 MONTH), '%Y-%m'),'-01') "
        "AND a.AuditTime >= '2023-09-07 15:17:00' "
        "AND a.InStockType IN ('IN0','IN1','IN3','IN5','IN6','IN8','IN9','IN10','IN11','IN12')"
        "AND NOT EXISTS(SELECT 1 FROM wh_stockagedetail s WHERE s.StockId=a.Id AND s.MaterialId=b.MaterialId) UNION "
        "SELECT ah.Id,bh.Id,ah.InStockNo,ah.FromWarehouseId,ah.FromWarehouseName,ah.MainPartId, "
        "eh.Name AS MainpartName,ah.WarehouseId,bh.MaterialId,bh.MaterialName, "
        "bh.MaterialType,bh.MaterialTypeCode,0 AS InOrOut,ah.AuditState,ah.InStockType "
        "AS StockType,IFNULL(ch.OwnerId,bh.OwnerId) AS OwnerId,fh.Name AS OwnerName, "
        "fh.ShortName AS OwnerShortName,ah.AuditTime,COALESCE(dh.StockNum,ch.InStockNum, "
        "bh.InStockedNum) AS StockNum,dh.StockPrice,dh.TaxPoint, CURDATE() "
        "FROM tb_instockinfohis ah "
        "INNER JOIN tb_instockdetailhis bh ON bh.InStockId=ah.Id "
        "AND bh.Deleted=0 AND bh.IsCodeSingle=0  "
        "LEFT JOIN tb_instockdetailextra ch "
        "ON ch.InStockDetailId=bh.Id AND ch.Deleted=0 "
        "LEFT JOIN tb_purchasedetailextra dh ON dh.StockDetailId=bh.Id "
        "AND dh.Deleted=0 LEFT JOIN tb_mainpartinfo eh "
        "ON eh.ProductCenterId=ah.MainPartId AND eh.Deleted=0 "
        "LEFT JOIN tb_ownerinfo fh ON fh.Id=IFNULL(ch.OwnerId,bh.OwnerId) "
        "AND fh.Deleted=0 WHERE ah.AuditState = 2 AND ah.Deleted=0 "
        "AND ah.InStockType IN ('IN0','IN1','IN3','IN5','IN6','IN8','IN9','IN10','IN11','IN12')"
        "AND ah.AuditTime < CONCAT(DATE_FORMAT(CURDATE(), '%Y-%m'),'-01') "
        # "AND ah.AuditTime >= CONCAT(DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -1 MONTH), '%Y-%m'),'-01') "
        "AND ah.AuditTime >= '2023-09-07 15:17:00' "
        "AND NOT EXISTS(SELECT 1 FROM wh_stockagedetail sh WHERE sh.StockId=ah.Id AND sh.MaterialId=bh.MaterialId) "
        "ORDER BY AuditTime DESC;"
    )
    sql_outstock_ins = (
        "INSERT INTO wh_stockagedetail (StockId,DetailId,StockNo,MainPartId,MainpartName, "
        "WarehouseId,MaterialId,MaterialName,MaterialType,MaterialTypeCode,InOrOut,AuditState, "
        "StockType,OwnerId,OwnerName,OwnerShortName,AuditTime,StockNum,StockPrice,TaxPoint,CheckDate) "
        "SELECT ao.Id,bo.Id,ao.OutStockNo,ao.MainPartId,eo.Name AS MainpartName,ao.WarehouseId, "
        "bo.MaterialId,bo.MaterialName,bo.MaterialType,bo.MaterialTypeCode,1 AS InOrOut, "
        "ao.AuditState,ao.OutStockType AS StockType,IFNULL(co.OwnerId,bo.OwnerId) "
        "AS OwnerId,fo.Name AS OwnerName,fo.ShortName AS OwnerShortName,ao.AuditTime,"
        "IFNULL(co.OutStockNum,bo.OutStockedNum)AS StockNum,NULL AS StockPrice,"
        "NULL AS TaxPoint,CURDATE() FROM tb_outstockinfo ao "
        "INNER JOIN tb_outstockdetail bo ON bo.OutStockId=ao.Id AND bo.Deleted=0 "
        "AND bo.IsCodeSingle=0 LEFT JOIN tb_outstockdetailextra co "
        "ON co.OutStockDetailId=bo.Id AND co.Deleted=0 "
        "LEFT JOIN tb_mainpartinfo eo "
        "ON eo.ProductCenterId=ao.MainPartId AND eo.Deleted=0 "
        "LEFT JOIN tb_ownerinfo fo ON fo.Id = IFNULL(co.OwnerId,bo.OwnerId) "
        "AND fo.Deleted=0 WHERE ao.AuditState IN (3,4) AND ao.Deleted=0 "
        "AND ao.AuditTime < CONCAT(DATE_FORMAT(CURDATE(), '%Y-%m'),'-01') "
        # "AND ao.AuditTime >= CONCAT(DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -1 MONTH), '%Y-%m'),'-01') "
        "AND ao.AuditTime >= '2023-09-07 15:17:00' "
        "AND ao.OutStockType IN ('OT0','OT1','OT3','OT5','OT6','OT8','OT9','OT10','OT11','OT12','OT13','OT14') "
        "AND NOT EXISTS(SELECT 1 FROM wh_stockagedetail so WHERE so.StockId=ao.Id AND so.MaterialId=bo.MaterialId) "
        "UNION "
        "SELECT aoh.Id,boh.Id,aoh.OutStockNo,aoh.MainPartId,eoh.Name AS MainpartName,aoh.WarehouseId, "
        "boh.MaterialId,boh.MaterialName,boh.MaterialType,boh.MaterialTypeCode,1 AS InOrOut, "
        "aoh.AuditState,aoh.OutStockType AS StockType,IFNULL(coh.OwnerId,boh.OwnerId) AS OwnerId,"
        "foh.Name AS OwnerName,foh.ShortName AS OwnerShortName,aoh.AuditTime,"
        "IFNULL(coh.OutStockNum,boh.OutStockedNum) AS StockNum, NULL AS StockPrice,NULL AS TaxPoint,CURDATE() "
        "FROM tb_outstockinfohis aoh "
        "INNER JOIN tb_outstockdetailhis boh ON boh.OutStockId=aoh.Id "
        "AND boh.Deleted=0 AND boh.IsCodeSingle=0 "
        "LEFT JOIN tb_outstockdetailextra coh ON coh.OutStockDetailId=boh.Id "
        "AND coh.Deleted=0 LEFT JOIN tb_mainpartinfo eoh ON eoh.ProductCenterId= "
        "aoh.MainPartId AND eoh.Deleted=0 LEFT JOIN tb_ownerinfo foh "
        "ON foh.Id=IFNULL(coh.OwnerId,boh.OwnerId) AND foh.Deleted=0 WHERE aoh.AuditState = 4 "
        "AND aoh.AuditTime < CONCAT(DATE_FORMAT(CURDATE(), '%Y-%m'),'-01') "
        # "AND aoh.AuditTime >= CONCAT(DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL -1 MONTH), '%Y-%m'),'-01') "
        "AND aoh.AuditTime >= '2023-09-07 15:17:00' "
        "AND aoh.OutStockType IN ('OT0','OT1','OT3','OT5','OT6','OT8','OT9','OT10','OT11','OT12','OT13','OT14') "
        "AND NOT EXISTS(SELECT 1 FROM wh_stockagedetail soh WHERE soh.StockId=aoh.Id AND soh.MaterialId=boh.MaterialId)"
        "ORDER BY AuditTime DESC;"
    )

    # 清除本次库存
    sql_inoutstock_del = "DELETE FROM wh_stockagedetail WHERE CheckDate = CURDATE();"

    # 获取货主库存
    sql_stock_del = "DELETE FROM wh_ownerstockinfo WHERE CheckDate = CURDATE();"
    sql_stock_ins = (
        "INSERT INTO wh_ownerstockinfo(Id, WarehouseId, MaterialId, MaterialName, "
        "OwnerId, StockNum, RealityNum, CheckDate) "
        "SELECT s.Id, s.WarehouseId, s.MaterialId, s.MaterialName, s.OwnerId, s.StockNum, "
        "s.RealityNum, CURDATE() FROM tb_ownerstockinfo s "
        "WHERE s.IsCodeSingle = 0 AND s.StockNum <> 0 AND s.Deleted = 0;"
    )
    # 写入明细
    cur.execute(sql_stock_del)
    sto_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print("明细结存、货主库存结余时间", sto_time)
    cur.execute(sql_stock_ins)
    cur.execute(sql_inoutstock_del)
    cur.execute(sql_instock_ins)
    cur.execute(sql_outstock_ins)

    # 检测是否有货主Id为空
    sql_own_ex = "SELECT Id FROM wh_stockagedetail WHERE CheckDate = CURDATE() AND OwnerId IS NULL; "
    sql_own_ex_fel = (
        "INSERT INTO wh_stockagefel(StockId, DetailId, StockNo, FromWarehouseId, FromWarehouseName, "
        "SupplierName, MainPartId, MainPartName, WarehouseId, MaterialId, MaterialName, MaterialType, "
        "MaterialTypeCode, InOrOut, AuditState, StockType, OwnerId, OwnerName, OwnerShortName, AuditTime, "
        "StockNum, StockPrice, TaxPoint, CheckDate, FelInfo) "
        "SELECT StockId, DetailId, StockNo,FromWarehouseId, FromWarehouseName, SupplierName, MainPartId, "
        "MainPartName, WarehouseId, MaterialId, MaterialName, MaterialType, MaterialTypeCode, InOrOut, "
        "AuditState, StockType, OwnerId, OwnerName, OwnerShortName, AuditTime, StockNum, StockPrice, "
        "TaxPoint, CURDATE(),%s FROM wh_stockagedetail WHERE Id = %s;"
    )
    cur.execute(sql_own_ex)
    own_ex = cur.fetchone()
    own_ex_id = own_ex.get("Id")
    if own_ex_id:
        cur.execute(sql_own_ex_fel, ["无业务主体！", own_ex_id])

    # 更新采购入库、采购换入的供应商
    sql_supplier = (
        "UPDATE wh_stockagedetail a,"
        "tb_instockinfohis b,"
        "tb_purchaseinfo c,"
        "tb_purchasedetail d,"
        "tb_supplier e "
        "SET a.SupplierName=e.Name "
        "WHERE a.StockType IN ('IN0','IN8') "
        "AND a.StockId=b.Id "
        "AND b.OrderId=c.Id "
        "AND c.Id=d.PurchaseId "
        "AND e.Id=d.SupplierId "
        "AND d.MaterialId=a.MaterialId "
        "AND a.SupplierName IS NULL "
        "AND a.CheckDate = CURDATE() "
        "AND b.Deleted=0 "
        "AND c.Deleted=0 "
        "AND d.Deleted=0 "
        "AND e.Deleted=0;"
    )
    cur.execute(sql_supplier)

    # 备份基础表
    sql_sto_have = "SELECT 1 FROM wh_stockagebase_back WHERE CheckDate = CURDATE();"
    sql_sto_bak = (
        "INSERT INTO wh_stockagebase_back(Id, StockNo, MainPartId, MainPartName, SupplierName, OwnerId, "
        "OwnerName, OwnerShortName, StockType, InOrOut, WarehouseId, FromWarehouseId, FromWarehouseName, "
        "MaterialId, MaterialName, MaterialType, MaterialTypeCode, StockPrice, TaxPoint, FnStockTime, StockTime, "
        "StockNum, SurplusNum, CheckDate, LastUpdateAt, InsertTime) "
        "SELECT Id, StockNo, MainPartId, MainPartName, SupplierName, OwnerId, OwnerName, OwnerShortName, StockType, "
        "InOrOut, WarehouseId, FromWarehouseId, FromWarehouseName, MaterialId, MaterialName, MaterialType, "
        "MaterialTypeCode, StockPrice, TaxPoint, FnStockTime, StockTime, StockNum, SurplusNum, CURDATE(), "
        "LastUpdateAt, InsertTime FROM wh_stockagebase;"
    )
    cur.execute(sql_sto_have)
    is_have = cur.fetchall()
    if len(is_have) == 0:
        cur.execute(sql_sto_bak)

    # 退货单取税价
    sql_find_at = (
        "SELECT a.AuditTime AS FnStockTime "
        "FROM tb_purchaseinfo a "
        "INNER JOIN tb_purchasedetail b "
        "ON b.PurchaseId=a.Id "
        "AND b.MaterialId=%s "
        "AND b.Deleted=0 "
        "WHERE a.Deleted=0 AND a.PurchaseType IN ('CG0','CG3') "
        "AND a.AuditTime <= %s "
        "AND a.AuditState IN (3,4,8) "
        "ORDER BY a.AuditTime DESC LIMIT 1;"
    )

    # 分组获取数据，来每条循环
    sql_group = (
        "SELECT DISTINCT a.MaterialId, a.OwnerId "
        "FROM wh_stockagedetail a "
        "WHERE CheckDate = CURDATE(); "
    )
    cur.execute(sql_group)
    rows = cur.fetchall()

    # 定义通用SQL

    sql_base_ex = "SELECT 1 FROM wh_stockagebase WHERE MaterialId=%s AND OwnerId=%s AND StockNum>0; "
    sql_base_up = "UPDATE wh_stockagebase SET StockNum=%s WHERE Id=%s;"
    sql_base_sto = (
        "SELECT Id,StockNum FROM wh_stockagebase "
        "WHERE MaterialId=%s AND OwnerId=%s AND StockNum>0 ORDER BY StockTime"
    )
    sql_base_ex_fel = (
        "INSERT INTO wh_stockagefel(StockId, DetailId, StockNo, FromWarehouseId, FromWarehouseName, "
        "SupplierName, MainPartId, MainPartName, WarehouseId, MaterialId, MaterialName, MaterialType, "
        "MaterialTypeCode, InOrOut, AuditState, StockType, OwnerId, OwnerName, OwnerShortName, "
        "AuditTime, StockNum, StockPrice, TaxPoint, CheckDate, FelInfo) "
        "SELECT StockId, DetailId, StockNo,FromWarehouseId, FromWarehouseName, SupplierName, "
        "MainPartId,MainPartName, WarehouseId, MaterialId, MaterialName, MaterialType, "
        "MaterialTypeCode, InOrOut,AuditState, StockType, OwnerId, OwnerName, OwnerShortName, "
        "AuditTime, StockNum, StockPrice,TaxPoint, CURDATE(),%s FROM wh_stockagedetail WHERE Id = %s;"
    )
    sql_base_ins = (
        "INSERT INTO wh_stockagebase(StockNo, MainPartId, MainPartName, SupplierName, OwnerId, OwnerName,"
        " OwnerShortName, StockType, InOrOut, WarehouseId, FromWarehouseId, FromWarehouseName, MaterialId,"
        " MaterialName, MaterialType, MaterialTypeCode, StockPrice, TaxPoint, FnStockTime, StockTime, "
        "StockNum, SurplusNum) "
        "SELECT StockNo, MainPartId, MainPartName, SupplierName, OwnerId, OwnerName, OwnerShortName, "
        "StockType, InOrOut, WarehouseId, FromWarehouseId, FromWarehouseName, MaterialId, MaterialName, "
        "MaterialType, MaterialTypeCode, StockPrice, TaxPoint, AuditTime, AuditTime, StockNum, StockNum "
        "FROM wh_stockagedetail WHERE Id=%s;"
    )

    for row in rows:
        r_mi = row.get("MaterialId")
        r_oi = row.get("OwnerId")

        # 逐条获取分组后的每条物料
        sql_search = "SELECT * FROM wh_stockagedetail WHERE CheckDate = CURDATE() "
        params = []
        sql_search += "AND MaterialId = %s "
        params.append(r_mi)
        if r_oi:
            sql_search += "AND OwnerId = %s "
            params.append(r_oi)
        sql_search += "ORDER BY AuditTime DESC, StockPrice DESC; "
        print('当前处理物料："' + r_mi + '",货主："' + str(r_oi) + '"')
        cur.execute(sql_search, params)
        res_sin = cur.fetchall()
        for res_row in res_sin:
            res_id = res_row.get("Id")
            res_sno = res_row.get("StockNo")
            res_fwi = res_row.get("FromWarehouseId")
            res_fwn = res_row.get("FromWarehouseName")
            res_mpi = res_row.get("MainPartId")
            res_mpn = res_row.get("MainPartName")
            res_su = res_row.get("SupplierName")
            res_oi = res_row.get("OwnerId")
            res_on = res_row.get("OwnerName")
            res_osn = res_row.get("OwnerShortName")
            res_wi = res_row.get("WarehouseId")
            res_mi = res_row.get("MaterialId")
            res_mn = res_row.get("MaterialName")
            res_mt = res_row.get("MaterialType")
            res_mtc = res_row.get("MaterialTypeCode")
            res_io = res_row.get("InOrOut")
            res_as = res_row.get("AuditState")
            res_st = res_row.get("StockType")
            res_at = res_row.get("AuditTime")
            res_sn = res_row.get("StockNum")
            res_mp = res_row.get("StockPrice")
            res_tx = res_row.get("TaxPoint")

            sn_sup = res_sn  # 剩余所需扣减库存
            sn_up = 0  # 本次所需扣减库存

            # 出库单处理
            if res_io == 1:
                # 判断基础表内是否有库存
                cur.execute(sql_base_ex, [res_mi, res_oi])
                base_ex = cur.fetchall()
                if not base_ex:
                    # 写入错误信息
                    cur.execute(sql_base_ex_fel, ["base表内无库存数据或库存不足！", res_id])

                # 查询有库龄库存的库存信息，按先进先出原则逐一扣减
                cur.execute(sql_base_sto, [res_mi, res_oi])
                base_sto = cur.fetchall()
                for res_base_sto in base_sto:
                    base_id = res_base_sto.get("Id")
                    base_sn = res_base_sto.get("StockNum")

                    # 判断出库单库存与库存
                    if sn_sup > 0:
                        if sn_sup >= base_sn:
                            sn_sup -= base_sn
                            sn_up = base_sn
                        else:
                            sn_up = base_sn - sn_sup
                            sn_sup = 0
                        # 更新base表库存
                        cur.execute(sql_base_up, [sn_up, base_id])
            # 入库单处理
            if res_io == 0:
                # 写入base表
                cur.execute(sql_base_ins, [res_id])

        con.commit()
        use_time = datetime.now() - loop_time
        use_times = use_time.total_seconds()
        loop_time = datetime.now()
        print("本物料、业务主体耗时： ", use_times, " 秒！")
    con.commit()
    total_time = datetime.now() - cu_time
    total_times = total_time.total_seconds()
    print("总计耗时： ", total_times, " 秒！")
except Exception as e:
    con.rollback()
    print(e)
finally:
    cur.close()
    con.close()
