#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw
# @generate at 2023/10/11 17:11

import mysql.connector
import configparser
from datetime import datetime

config = configparser.ConfigParser()
config.read("db.conf")

my_host = config.get("whpro", "host")
my_database = config.get("whpro", "database")
my_user = config.get("whpro", "user")
my_password = config.get("whpro", "password")

con = mysql.connector.connect(
    host=my_host, user=my_user, password=my_password, database=my_database
)
cu_time = datetime.now()
cu_date = cu_time.date()
loop_time = datetime.now()

try:
    cur = con.cursor(dictionary=True)

    sql_find_at = (
        "SELECT a.AuditTime AS FnStockTime "
        "FROM tb_purchaseinfo a "
        "INNER JOIN tb_purchasedetail b "
        "ON b.PurchaseId=a.Id "
        "AND b.MaterialId=%s "
        "AND b.Deleted=0 "
        "WHERE a.Deleted=0 AND a.PurchaseType IN ('CG0','CG3') "
        "AND a.AuditTime <= %s "
        "AND a.AuditState IN (3,4,8) "
        "ORDER BY a.AuditTime DESC LIMIT 1;"
    )

    # 分组获取数据，来每条循环
    sql_group = (
        "SELECT DISTINCT a.MaterialId, a.OwnerId "
        "FROM wh_stockagedetail a "
        "WHERE EXISTS(SELECT 1 FROM wh_ownerstockinfo b "
        "WHERE b.MaterialId=a.MaterialId) "
        "AND StockType NOT IN ('IN2','IN4','IN7','OT2','OT4','OT7'); "
    )
    cur.execute(sql_group)
    rows = cur.fetchall()
    for row in rows:
        r_mi = row.get("MaterialId")
        r_oi = row.get("OwnerId")

        # 获取仓-物料的当前库内库存
        sql_cur_stock = (
            "SELECT MaterialId, OwnerId, SUM(StockNum) AS StockNum "
            "FROM wh_ownerstockinfo "
            "WHERE MaterialId = %s "
            "AND CheckDate = '2023-11-01' "
            "AND OwnerId = %s "
            "GROUP BY MaterialId, OwnerId;"
        )
        cur.execute(sql_cur_stock, [r_mi, r_oi])
        cu_sn = cur.fetchall()

        if len(cu_sn) > 0:
            cu_sn = int(cu_sn[0].get("StockNum"))
            mi_num = 0
            need_num = cu_sn
            print("当前在库库存： {}".format(cu_sn))

            # 逐条获取分组后的每条物料
            sql_search = (
                "SELECT * "
                "FROM wh_stockagedetail a "
                "WHERE StockType NOT IN ('IN2','IN4','IN7','OT2','OT4','OT7') "
            )
            params = []
            sql_search += "AND a.MaterialId = %s "
            params.append(r_mi)
            if r_oi:
                sql_search += "AND a.OwnerId = %s "
                params.append(r_oi)
            sql_search += (
                "AND NOT EXISTS(SELECT 1 FROM wh_stockagebase b WHERE b.Id=a.Id) "
                "ORDER BY a.AuditTime DESC, a.StockPrice DESC ;"
            )
            print('当前处理物料："' + r_mi + '",货主："' + str(r_oi) + '"')
            cur.execute(sql_search, params)
            res_sin = cur.fetchall()
            for res_row in res_sin:
                if need_num > 0:
                    res_id = res_row.get("Id")
                    res_sno = res_row.get("StockNo")
                    res_fwi = res_row.get("FromWarehouseId")
                    res_fwn = res_row.get("FromWarehouseName")
                    res_mpi = res_row.get("MainPartId")
                    res_mpn = res_row.get("MainPartName")
                    res_su = res_row.get("SupplierName")
                    res_oi = res_row.get("OwnerId")
                    res_on = res_row.get("OwnerName")
                    res_osn = res_row.get("OwnerShortName")
                    res_wi = res_row.get("WarehouseId")
                    res_mi = res_row.get("MaterialId")
                    res_mn = res_row.get("MaterialName")
                    res_mt = res_row.get("MaterialType")
                    res_mtc = res_row.get("MaterialTypeCode")
                    res_io = res_row.get("InOrOut")
                    res_as = res_row.get("AuditState")
                    res_st = res_row.get("StockType")
                    res_at = res_row.get("AuditTime")
                    res_sn = res_row.get("StockNum")
                    res_mp = res_row.get("StockPrice")
                    res_tx = res_row.get("TaxPoint")

                    # print("单据时间：" + res_at.strftime("%Y-%m-%d"))
                    # print("单据类型：" + res_st)
                    # print("单据数量：" + str(res_sn))

                    # 计算仓库库龄
                    if need_num > 0:
                        # 需要重新计算仓库库龄的, +
                        if res_io == 0:
                            if need_num > res_sn:
                                mi_num = res_sn
                                need_num -= res_sn
                                if cu_sn > res_sn:
                                    cu_sn -= res_sn
                                else:
                                    cu_sn = 0
                            else:
                                mi_num = need_num
                                need_num = 0
                            # print("本次入后所需库存量：" + str(cu_sn))
                            # print("本次入后库龄库存所需量：" + str(need_num))
                            # print("本次入实际消耗库龄库存：" + str(mi_num))
                            if res_st == "IN3":
                                cur.execute(sql_find_at, [res_mi, res_at])
                                sql_res = cur.fetchall()
                                if sql_res:
                                    in_at = sql_res[0].get("FnStockTime")
                                else:
                                    in_at = res_at
                            else:
                                in_at = res_at
                            params_in = [
                                res_id,
                                res_sno,
                                res_mpi,
                                res_mpn,
                                res_su,
                                res_oi,
                                res_on,
                                res_osn,
                                res_st,
                                res_io,
                                res_wi,
                                res_fwi,
                                res_fwn,
                                res_mi,
                                res_mn,
                                res_mt,
                                res_mtc,
                                res_mp,
                                res_tx,
                                in_at,
                                res_at,
                                res_sn,
                                mi_num,
                            ]
                            sql_in = (
                                "INSERT INTO wh_stockagebase (Id, StockNo, MainPartId, MainPartName, "
                                "SupplierName, OwnerId, OwnerName, OwnerShortName, StockType, InOrOut, "
                                "WarehouseId, FromWarehouseId, FromWarehouseName, MaterialId, "
                                "MaterialName, MaterialType, MaterialTypeCode, StockPrice, TaxPoint, "
                                "FnStockTime, StockTime, StockNum, SurplusNum) "
                                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                                "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                            )
                            cur.execute(sql_in, params_in)

                        # 需要重新计算仓库库龄的, -
                        if res_io == 1:
                            cu_sn += res_sn
                            print("本次出后所需库存量：" + str(cu_sn))
                            params_ot = [
                                res_id,
                                res_sno,
                                res_mpi,
                                res_mpn,
                                res_su,
                                res_oi,
                                res_on,
                                res_osn,
                                res_st,
                                res_io,
                                res_wi,
                                res_fwi,
                                res_fwn,
                                res_mi,
                                res_mn,
                                res_mt,
                                res_mtc,
                                res_mp,
                                res_tx,
                                res_at,
                                res_sn,
                            ]
                            sql_ot = (
                                "INSERT INTO wh_stockagebase (Id, StockNo, MainPartId, MainPartName, "
                                "SupplierName, OwnerId, OwnerName, OwnerShortName, StockType, InOrOut, "
                                "WarehouseId, FromWarehouseId, FromWarehouseName, MaterialId, "
                                "MaterialName, MaterialType, MaterialTypeCode, StockPrice, TaxPoint, "
                                "StockTime, StockNum) "
                                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                                "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                            )
                            cur.execute(sql_ot, params_ot)
                            con.commit()
                        print("仓库库龄剩余需扣库存量：" + str(need_num))
            use_time = datetime.now() - loop_time
            use_times = use_time.total_seconds()
            loop_time = datetime.now()
            print("本物料、业务主体耗时： ", use_times, " 秒！")
    con.commit()
    total_time = datetime.now() - cu_time
    total_times = total_time.total_seconds()
    print("总计耗时： ", total_times, " 秒！")
except Exception as e:
    con.rollback()
    print(e)
finally:
    cur.close()
    con.close()
