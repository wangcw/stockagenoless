# -*- coding:utf-8 -*-
# @author by wangcw
# @generate at 2023/7/25 13:59

import mysql.connector
import configparser
from datetime import datetime

config = configparser.ConfigParser()
config.read('db.conf')

my_host = config.get('test', 'host')
my_database = config.get('test', 'database')
my_user = config.get('test', 'user')
my_password = config.get('test', 'password')

con = mysql.connector.connect(
    host=my_host,
    user=my_user,
    password=my_password,
    database=my_database
)
cu_time = datetime.now()
cu_date = cu_time.date()
loop_time = datetime.now()

try:
    cur = con.cursor(dictionary=True)

    # 创建每条单据计算临时表
    sql_sto_dro = 'DROP TABLE IF EXISTS wh_stockagecur;'
    sql_sto_cre = 'CREATE TABLE wh_stockagecur (' \
                  'Id bigint unsigned NOT NULL AUTO_INCREMENT COMMENT \'自增主键\' PRIMARY KEY, ' \
                  'StockNo varchar(100) COMMENT \'出入库单号\', ' \
                  'MainPartId varchar(100) COMMENT \'销售平台Id\', ' \
                  'MainPartName varchar(100) COMMENT \'销售平台名称\', ' \
                  'SupplierName varchar(100) COMMENT \'供应商名称\', ' \
                  'OwnerId varchar(100) COMMENT \'货主Id\', ' \
                  'OwnerName varchar(100) COMMENT \'货主名称\', ' \
                  'OwnerShortName varchar(100) COMMENT \'货主简称\', ' \
                  'StockType varchar(100) COMMENT \'出入库类型\', ' \
                  'WarehouseId varchar(100) COMMENT \'所在二级仓Id\', ' \
                  'FromWarehouseId varchar(100) COMMENT \'所在二级仓Id\', ' \
                  'FromWarehouseName varchar(100) COMMENT \'所在二级仓名称\', ' \
                  'MaterialId varchar(100) COMMENT \'物料Id\', ' \
                  'MaterialName varchar(100) COMMENT \'物料名称\', ' \
                  'MaterialType varchar(100) COMMENT \'物料类型\', ' \
                  'MaterialTypeCode varchar(100) COMMENT \'物料类型编码\', ' \
                  'IsTransport smallint COMMENT \'是否在途\', ' \
                  'StockPrice decimal(18,2) COMMENT \'采购单价\', ' \
                  'TaxPoint int COMMENT \'采购税率\', ' \
                  'FnStockAge int COMMENT \'财务管理库龄\', ' \
                  'FnStockTime datetime COMMENT \'财务库龄入库时间\', ' \
                  'StockAge int COMMENT \'仓库库龄\', ' \
                  'StockTime datetime COMMENT \'仓库库龄入库时间\', ' \
                  'StockNum int,' \
                  'KEY `1` (OwnerId), ' \
                  'KEY `2` (WarehouseId), ' \
                  'KEY `3` (MaterialId), ' \
                  'KEY `4` (StockType), ' \
                  'KEY `5` (FnStockTime), ' \
                  'KEY `6` (StockTime), ' \
                  'KEY `7` (StockNum) ' \
                  ');'
    # cur.execute(sql_sto_dro)
    # cur.execute(sql_sto_cre)

    # 分组获取数据，来每条循环
    sql_group = 'SELECT DISTINCT a.WarehouseId, a.MaterialId, a.OwnerId ' \
                'FROM wh_stockagedetail a WHERE EXISTS(SELECT 1 FROM tm_ownerstockinfo b ' \
                'WHERE b.MaterialId=a.MaterialId AND b.WarehouseId=a.WarehouseId); ' \
                # 'AND WarehouseId=\'WH9999988104\' AND MaterialId=\'MI0000013238\' AND OwnerId=\'ON9999999997\';'
    cur.execute(sql_group)
    rows = cur.fetchall()
    for row in rows:
        r_wi = row.get('WarehouseId')
        r_mi = row.get('MaterialId')
        r_oi = row.get('OwnerId')

        # 获取仓-物料的当前库内库存
        sql_cur_stock = 'SELECT WarehouseId, MaterialId, StockNum, RealityNum ' \
                        'FROM tm_ownerstockinfo ' \
                        'WHERE WarehouseId = %s ' \
                        'AND MaterialId = %s ' \
                        'AND OwnerId = %s;'
        cur.execute(sql_cur_stock, [r_wi, r_mi, r_oi])
        cu_sn = cur.fetchall()

        if len(cu_sn) > 0:
            cu_sn = int(cu_sn[0].get('RealityNum'))
            mi_num = 0
            need_num = cu_sn
            print('当前在库库存： {}'.format(cu_sn))

            # 逐条获取分组后的每条物料
            sql_search = 'SELECT * ' \
                         'FROM wh_stockagedetail a ' \
                         'WHERE 1=1 '
            params = []
            sql_search += 'AND a.WarehouseId = %s '
            params.append(r_wi)
            sql_search += 'AND a.MaterialId = %s '
            params.append(r_mi)
            if r_oi:
                sql_search += 'AND a.OwnerId = %s '
                params.append(r_oi)
            sql_search += 'AND NOT EXISTS(SELECT 1 FROM wh_stockagecur b WHERE b.Id=a.Id) ' \
                          'ORDER BY a.AuditTime DESC, a.StockPrice DESC, IFNULL(a.OwnerId,\'ON9999999999\') ;'
            print('当前处理物料："' + r_mi + '" ,仓库："' + r_wi + '",货主："' + str(r_oi) + '"')
            cur.execute(sql_search, params)
            res_sin = cur.fetchall()
            for res_row in res_sin:
                if need_num > 0:
                    res_id = res_row.get('Id')
                    res_sno = res_row.get('StockNo')
                    res_fwi = res_row.get('FromWarehouseId')
                    res_fwn = res_row.get('FromWarehouseName')
                    res_mpi = res_row.get('MainPartId')
                    res_mpn = res_row.get('MainPartName')
                    res_su = res_row.get('SupplierName')
                    res_oi = res_row.get('OwnerId')
                    res_on = res_row.get('OwnerName')
                    res_osn = res_row.get('OwnerShortName')
                    res_wi = res_row.get('WarehouseId')
                    res_mi = res_row.get('MaterialId')
                    res_mn = res_row.get('MaterialName')
                    res_mt = res_row.get('MaterialType')
                    res_mtc = res_row.get('MaterialTypeCode')
                    res_io = res_row.get('InOrOut')
                    res_as = res_row.get('AuditState')
                    res_st = res_row.get('StockType')
                    res_at = res_row.get('AuditTime')
                    res_sn = res_row.get('StockNum')
                    res_mp = res_row.get('StockPrice')
                    res_tx = res_row.get('TaxPoint')

                    print('单据时间：' + res_at.strftime('%Y-%m-%d'))
                    print('单据类型：' + res_st)
                    print('单据数量：' + str(res_sn))

                    # 计算仓库库龄
                    if need_num > 0:
                        # 需要重新计算仓库库龄的, +
                        if res_st != 'IN7' and res_io == 0:
                            if not (res_st == 'IN2' and res_as == 0):
                                if need_num > res_sn:
                                    mi_num = res_sn
                                    need_num -= res_sn
                                    if cu_sn - res_sn > 0:
                                        cu_sn -= res_sn
                                    else:
                                        cu_sn = 0
                                else:
                                    mi_num = need_num
                                    need_num = 0
                            print('本次入后所需库存量：' + str(cu_sn))
                            print('本次入后库龄库存所需量：' + str(need_num))
                            print('本次入实际消耗库龄库存：' + str(mi_num) + '入库单号：' + res_sno)

                        # 需要重新计算仓库库龄的, -
                        if res_st != 'OT7' and res_io == 1:
                            cu_sn += res_sn
                            print('本次出后所需库存量：' + str(cu_sn))
                        if res_io == 0:
                            if res_st == 'IN2' and res_as == 0:
                                is_tra = 1
                                mi_num = res_sn
                            else:
                                is_tra = 0
                            params_sto = [res_id, res_sno, res_mpi, res_mpn, res_su, res_oi, res_on, res_osn,
                                          res_st, res_wi, res_fwi, res_fwn, res_mi, res_mn, res_mt, res_mtc,
                                          is_tra, res_mp, res_tx, res_at, mi_num]
                            sql_sto_ins = 'INSERT INTO wh_stockagecur (Id, StockNo, MainPartId, MainPartName, ' \
                                          'SupplierName, OwnerId, OwnerName, OwnerShortName, StockType, ' \
                                          'WarehouseId, FromWarehouseId, FromWarehouseName, MaterialId, ' \
                                          'MaterialName, MaterialType, MaterialTypeCode, IsTransport, ' \
                                          'StockPrice, TaxPoint, StockTime, StockNum) ' \
                                          'VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, ' \
                                          '%s, %s, %s, %s, %s, %s, %s, %s, %s)'
                            cur.execute(sql_sto_ins, params_sto)
                            con.commit()
                            use_time = datetime.now() - loop_time
                            use_times = use_time.total_seconds()
                            loop_time = datetime.now()
                            print('本轮耗时： ', use_times, ' 秒！')

                print('仓库库龄剩余需扣库存量：' + str(need_num))
    # 更新全部财务库龄时间
    sql_update_fn = 'UPDATE wh_stockagecur x, ' \
                    '(SELECT a.MaterialId,a.AuditTime, ' \
                    'ROW_NUMBER() OVER(PARTITION BY a.MaterialId ORDER BY a.AuditTime) AS num ' \
                    'FROM wh_stockagedetail a ' \
                    'WHERE a.StockType NOT IN (\'IN2\',\'IN4\',\'IN10\',\'IN13\')) y ' \
                    'SET x.FnStockTime=y.AuditTime WHERE x.MaterialId=y.MaterialId AND y.num=1;'
    # cur.execute(sql_update_fn)
    # 更新财务库龄、在库李玲
    sql_up_fnage = 'UPDATE wh_stockagecur ' \
                   'SET FnStockAge = DATEDIFF(CURRENT_DATE(), FnStockTime) ' \
                   'WHERE 1=1;'
    sql_up_age = 'UPDATE wh_stockagecur ' \
                 'SET StockAge = DATEDIFF(CURRENT_DATE(), StockTime) ' \
                 'WHERE 1=1;'
    # cur.execute(sql_up_fnage)
    # cur.execute(sql_up_age)
    con.commit()
    total_time = datetime.now() - cu_time
    total_times = total_time.total_seconds()
    print('总计耗时： ', total_times, ' 秒！')
except Exception as e:
    con.rollback()
    print(e)
finally:
    cur.close()
    con.close()
