#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw
# @generate at 2023/9/1 09:17

import mysql.connector
import configparser
from datetime import datetime

config = configparser.ConfigParser()
config.read("db.conf")

my_host = config.get("whcenter", "host")
my_database = config.get("whcenter", "database")
my_user = config.get("whcenter", "user")
my_password = config.get("whcenter", "password")

con = mysql.connector.connect(
    host=my_host, user=my_user, password=my_password, database=my_database
)
cu_time = datetime.now()
cu_date = cu_time.date()
loop_time = datetime.now()

try:
    cur = con.cursor(dictionary=True)

    # 写入每月新明细数据
    # 写入物料流水数据
    sql_instock_ins = (
        "INSERT INTO wh_stockagedetail (StockId,DetailId,StockNo,FromWarehouseId,"
        "FromWarehouseName,MainPartId,MainpartName,WarehouseId,MaterialId,MaterialName,MaterialType, "
        "MaterialTypeCode,InOrOut,AuditState,StockType,OwnerId,OwnerName,OwnerShortName, "
        "AuditTime,StockNum,StockPrice,TaxPoint) SELECT a.Id,b.Id,a.InStockNo,a.FromWarehouseId, "
        "a.FromWarehouseName,a.MainPartId,e.Name AS MainpartName,a.WarehouseId, "
        "b.MaterialId,b.MaterialName,b.MaterialType,b.MaterialTypeCode,0 AS InOrOut, "
        "a.AuditState,a.InStockType AS StockType,IFNULL(c.OwnerId,b.OwnerId) AS OwnerId, "
        "f.Name AS OwnerName,f.ShortName AS OwnerShortName,a.AuditTime, "
        "COALESCE(d.StockNum,c.InStockNum,b.InStockedNum) AS StockNum,d.StockPrice, "
        "d.TaxPoint FROM tb_instockinfo a "
        "INNER JOIN tb_instockdetail b ON b.InStockId=a.Id "
        "AND b.Deleted=0 AND b.IsCodeSingle=0 "
        "LEFT JOIN tb_instockdetailextra c ON c.InStockDetailId=b.Id "
        "AND c.Deleted=0 LEFT JOIN tb_purchasedetailextra d "
        "ON d.StockDetailId=b.Id AND d.Deleted=0 LEFT JOIN tb_mainpartinfo e "
        "ON e.ProductCenterId=a.MainPartId AND e.Deleted=0 "
        "LEFT JOIN tb_ownerinfo f ON f.Id=IFNULL(c.OwnerId,b.OwnerId) "
        "AND f.Deleted=0 WHERE a.AuditState IN (0,2) AND a.Deleted=0 "
        "AND a.AuditTime > '2023-08-10' AND a.AuditTime <= %s "
        "AND a.InStockType IN ('IN0','IN1','IN3','IN5','IN6','IN8','IN9','IN10','IN11','IN12')"
        "AND NOT EXISTS(SELECT 1 FROM wh_stockagedetail s WHERE s.StockId=a.Id AND s.MaterialId=b.MaterialId) UNION "
        "SELECT ah.Id,bh.Id,ah.InStockNo,ah.FromWarehouseId,ah.FromWarehouseName,ah.MainPartId, "
        "eh.Name AS MainpartName,ah.WarehouseId,bh.MaterialId,bh.MaterialName, "
        "bh.MaterialType,bh.MaterialTypeCode,0 AS InOrOut,ah.AuditState,ah.InStockType "
        "AS StockType,IFNULL(ch.OwnerId,bh.OwnerId) AS OwnerId,fh.Name AS OwnerName, "
        "fh.ShortName AS OwnerShortName,ah.AuditTime,COALESCE(dh.StockNum,ch.InStockNum, "
        "bh.InStockedNum) AS StockNum,dh.StockPrice,dh.TaxPoint "
        "FROM tb_instockinfohis ah "
        "INNER JOIN tb_instockdetailhis bh ON bh.InStockId=ah.Id "
        "AND bh.Deleted=0 AND bh.IsCodeSingle=0  "
        "LEFT JOIN tb_instockdetailextra ch "
        "ON ch.InStockDetailId=bh.Id AND ch.Deleted=0 "
        "LEFT JOIN tb_purchasedetailextra dh ON dh.StockDetailId=bh.Id "
        "AND dh.Deleted=0 LEFT JOIN tb_mainpartinfo eh "
        "ON eh.ProductCenterId=ah.MainPartId AND eh.Deleted=0 "
        "LEFT JOIN tb_ownerinfo fh ON fh.Id=IFNULL(ch.OwnerId,bh.OwnerId) "
        "AND fh.Deleted=0 WHERE ah.AuditState = 2 AND ah.Deleted=0 "
        "AND ah.InStockType IN ('IN0','IN1','IN3','IN5','IN6','IN8','IN9','IN10','IN11','IN12')"
        "AND ah.AuditTime > '2023-08-10' AND ah.AuditTime <= %s "
        "AND NOT EXISTS(SELECT 1 FROM wh_stockagedetail sh WHERE sh.StockId=ah.Id AND sh.MaterialId=bh.MaterialId) "
        "ORDER BY AuditTime DESC;"
    )
    sql_outstock_ins = (
        "INSERT INTO wh_stockagedetail (StockId,DetailId,StockNo,MainPartId,MainpartName, "
        "WarehouseId,MaterialId,MaterialName,MaterialType,MaterialTypeCode,InOrOut,AuditState, "
        "StockType,OwnerId,OwnerName,OwnerShortName,AuditTime,StockNum,StockPrice,TaxPoint) "
        "SELECT ao.Id,bo.Id,ao.OutStockNo,ao.MainPartId,eo.Name AS MainpartName,ao.WarehouseId, "
        "bo.MaterialId,bo.MaterialName,bo.MaterialType,bo.MaterialTypeCode,1 AS InOrOut, "
        "ao.AuditState,ao.OutStockType AS StockType,IFNULL(co.OwnerId,bo.OwnerId) "
        "AS OwnerId,fo.Name AS OwnerName,fo.ShortName AS OwnerShortName,ao.AuditTime,"
        "IFNULL(co.OutStockNum,bo.OutStockedNum)AS StockNum,NULL AS StockPrice,"
        "NULL AS TaxPoint FROM tb_outstockinfo ao "
        "INNER JOIN tb_outstockdetail bo ON bo.OutStockId=ao.Id AND bo.Deleted=0 "
        "AND bo.IsCodeSingle=0 LEFT JOIN tb_outstockdetailextra co "
        "ON co.OutStockDetailId=bo.Id AND co.Deleted=0 "
        "LEFT JOIN tb_mainpartinfo eo "
        "ON eo.ProductCenterId=ao.MainPartId AND eo.Deleted=0 "
        "LEFT JOIN tb_ownerinfo fo ON fo.Id = IFNULL(co.OwnerId,bo.OwnerId) "
        "AND fo.Deleted=0 WHERE ao.AuditState IN (3,4) AND ao.Deleted=0 "
        "AND ao.AuditTime > '2023-08-10' AND ao.AuditTime <= %s "
        "AND ao.OutStockType IN ('OT0','OT1','OT3','OT5','OT6','OT8','OT9','OT10','OT11','OT12','OT13','OT14') "
        "AND NOT EXISTS(SELECT 1 FROM wh_stockagedetail so WHERE so.StockId=ao.Id AND so.MaterialId=bo.MaterialId) UNION "
        "SELECT aoh.Id,boh.Id,aoh.OutStockNo,aoh.MainPartId,eoh.Name AS MainpartName,aoh.WarehouseId, "
        "boh.MaterialId,boh.MaterialName,boh.MaterialType,boh.MaterialTypeCode,1 AS InOrOut, "
        "aoh.AuditState,aoh.OutStockType AS StockType,IFNULL(coh.OwnerId,boh.OwnerId) AS OwnerId,"
        "foh.Name AS OwnerName,foh.ShortName AS OwnerShortName,aoh.AuditTime,"
        "IFNULL(coh.OutStockNum,boh.OutStockedNum) AS StockNum, NULL AS StockPrice,NULL AS TaxPoint "
        "FROM tb_outstockinfohis aoh "
        "INNER JOIN tb_outstockdetailhis boh ON boh.OutStockId=aoh.Id "
        "AND boh.Deleted=0 AND boh.IsCodeSingle=0 "
        "LEFT JOIN tb_outstockdetailextra coh ON coh.OutStockDetailId=boh.Id "
        "AND coh.Deleted=0 LEFT JOIN tb_mainpartinfo eoh ON eoh.ProductCenterId= "
        "aoh.MainPartId AND eoh.Deleted=0 LEFT JOIN tb_ownerinfo foh "
        "ON foh.Id=IFNULL(coh.OwnerId,boh.OwnerId) AND foh.Deleted=0 WHERE aoh.AuditState = 4 "
        "AND aoh.Deleted=0 AND aoh.AuditTime > '2023-08-10'  AND aoh.AuditTime <= %s "
        "AND aoh.OutStockType IN ('OT0','OT1','OT3','OT5','OT6','OT8','OT9','OT10','OT11','OT12','OT13','OT14') "
        "AND NOT EXISTS(SELECT 1 FROM wh_stockagedetail soh WHERE soh.StockId=aoh.Id AND soh.MaterialId=boh.MaterialId)"
        "ORDER BY AuditTime DESC;"
    )

    # 获取货主库存
    sql_stock_dro = "DROP TABLE IF EXISTS wh_ownerstockinfo;"
    sql_stock_cre = (
        "CREATE TABLE wh_ownerstockinfo ( "
        "Id bigint NOT NULL AUTO_INCREMENT COMMENT '自增主键', "
        "WarehouseId varchar(100) COMMENT '仓库Id(tb_warehouse.Id)', "
        "MaterialId varchar(100) COMMENT '物料Id', "
        "MaterialName varchar(100) COMMENT '物料名称', "
        "OwnerId varchar(100) COMMENT '货主Id', "
        "StockNum int COMMENT '在库数量', "
        "RealityNum int COMMENT '可用库存', "
        "primary key (id), "
        "key `1` (WarehouseId), "
        "key `2` (MaterialId), "
        "key `3` (OwnerId) "
        ");"
    )
    sql_stock_ins = (
        "INSERT INTO wh_ownerstockinfo(Id, WarehouseId, MaterialId, "
        "OwnerId, StockNum, RealityNum) SELECT Id, WarehouseId, MaterialId, OwnerId, StockNum, "
        "RealityNum FROM tb_ownerstockinfo "
        "WHERE IsCodeSingle = 0 AND StockNum <> 0 AND Deleted = 0;"
    )
    table_start = datetime.now()
    # 写入明细
    sto_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print("明细结存、货主库存结余时间", sto_time)
    cur.execute(sql_instock_ins, [sto_time, sto_time])
    cur.execute(sql_outstock_ins, [sto_time, sto_time])

    # 更新采购入库、采购换入的供应商
    sql_supplier = (
        "UPDATE wh_stockagedetail a,"
        "tb_instockinfohis b,"
        "tb_purchaseinfo c,"
        "tb_purchasedetail d,"
        "tb_supplier e "
        "SET a.SupplierName=e.Name "
        "WHERE a.StockType IN ('IN0','IN') "
        "AND a.StockId=b.Id "
        "AND b.OrderId=c.Id "
        "AND c.Id=d.PurchaseId "
        "AND e.Id=d.SupplierId "
        "AND d.MaterialId=a.MaterialId "
        "AND a.SupplierName IS NULL "
        "AND b.Deleted=0 "
        "AND c.Deleted=0 "
        "AND d.Deleted=0 "
        "AND e.Deleted=0;"
    )
    cur.execute(sql_supplier)

    # 创建每条单据计算临时表
    sql_sto_dro = "DROP TABLE IF EXISTS wh_stockagebase;"
    sql_sto_cre = (
        "CREATE TABLE wh_stockagebase ("
        "Id bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键' PRIMARY KEY, "
        "StockNo varchar(100) COMMENT '出入库单号', "
        "MainPartId varchar(100) COMMENT '销售平台Id', "
        "MainPartName varchar(100) COMMENT '销售平台名称', "
        "SupplierName varchar(100) COMMENT '供应商名称', "
        "OwnerId varchar(100) COMMENT '货主Id', "
        "OwnerName varchar(100) COMMENT '货主名称', "
        "OwnerShortName varchar(100) COMMENT '货主简称', "
        "StockType varchar(100) COMMENT '出入库类型', "
        "InOrOut smallint COMMENT '出入库类型(0入1出)', "
        "WarehouseId varchar(100) COMMENT '所在二级仓Id', "
        "FromWarehouseId varchar(100) COMMENT '所在二级仓Id', "
        "FromWarehouseName varchar(100) COMMENT '所在二级仓名称', "
        "MaterialId varchar(100) COMMENT '物料Id', "
        "MaterialName varchar(100) COMMENT '物料名称', "
        "MaterialType varchar(100) COMMENT '物料类型', "
        "MaterialTypeCode varchar(100) COMMENT '物料类型编码', "
        "StockPrice decimal(18,2) COMMENT '采购单价', "
        "TaxPoint int COMMENT '采购税率', "
        "FnStockTime datetime COMMENT '财务库龄入库时间', "
        "StockTime datetime COMMENT '仓库库龄入库时间', "
        "StockNum int COMMENT '单据库存量', "
        "SurplusNum int NOT NULL DEFAULT 0 COMMENT '单据库存剩余量', "
        "KEY `1` (OwnerId), "
        "KEY `2` (WarehouseId), "
        "KEY `3` (MaterialId), "
        "KEY `4` (StockType), "
        "KEY `5` (FnStockTime), "
        "KEY `6` (StockTime), "
        "KEY `7` (StockNum), "
        "KEY `8` (InOrOut) "
        ");"
    )
    cur.execute(sql_sto_dro)
    cur.execute(sql_sto_cre)

    # 退货单取税价
    sql_find_at = (
        "SELECT a.AuditTime AS FnStockTime "
        "FROM tb_purchaseinfo a "
        "INNER JOIN tb_purchasedetail b "
        "ON b.PurchaseId=a.Id "
        "AND b.MaterialId=%s "
        "AND b.Deleted=0 "
        "WHERE a.Deleted=0 AND a.PurchaseType IN ('CG0','CG3') "
        "AND a.AuditTime <= %s "
        "AND a.AuditState IN (3,4,8) "
        "ORDER BY a.AuditTime DESC LIMIT 1;"
    )

    # 分组获取数据，来每条循环
    sql_group = (
        "SELECT DISTINCT a.MaterialId, a.OwnerId "
        "FROM wh_stockagedetail a "
        "WHERE EXISTS(SELECT 1 FROM wh_ownerstockinfo b "
        "WHERE b.MaterialId=a.MaterialId) "
        "AND StockType NOT IN ('IN2','IN4','IN7','IN13','IN14', "
        "'OT2','OT4','OT7','OT15','OT16'); "
    )
    # 'AND MaterialId=\'MI0000006563\' AND OwnerId=\'ON9999999998\' ' \

    cur.execute(sql_group)
    rows = cur.fetchall()
    for row in rows:
        r_mi = row.get("MaterialId")
        r_oi = row.get("OwnerId")

        # 获取仓-物料的当前库内库存
        sql_cur_stock = (
            "SELECT MaterialId, OwnerId, SUM(StockNum) AS StockNum "
            "FROM wh_ownerstockinfo "
            "WHERE MaterialId = %s "
            "AND OwnerId = %s "
            "GROUP BY MaterialId, OwnerId;"
        )
        # 'AND WarehouseId = %s ' \
        cur.execute(sql_cur_stock, [r_mi, r_oi])
        cu_sn = cur.fetchall()

        if len(cu_sn) > 0:
            cu_sn = int(cu_sn[0].get("StockNum"))
            mi_num = 0
            need_num = cu_sn
            print("当前在库库存： {}".format(cu_sn))

            # 逐条获取分组后的每条物料
            sql_search = (
                "SELECT * "
                "FROM wh_stockagedetail a "
                "WHERE StockType NOT IN ('IN2','IN4','IN7','IN13','IN14', "
                "'OT2','OT4','OT7','OT15','OT16') "
            )
            params = []
            sql_search += "AND a.MaterialId = %s "
            params.append(r_mi)
            if r_oi:
                sql_search += "AND a.OwnerId = %s "
                params.append(r_oi)
            sql_search += (
                "AND NOT EXISTS(SELECT 1 FROM wh_stockagebase b WHERE b.Id=a.Id) "
                "ORDER BY a.AuditTime DESC, a.StockPrice DESC ;"
            )
            # 'AND MaterialId = \'MI0000006563\' AND OwnerId = \'ON9999999998\' ' \

            print('当前处理物料："' + r_mi + '",货主："' + str(r_oi) + '"')
            cur.execute(sql_search, params)
            res_sin = cur.fetchall()
            for res_row in res_sin:
                if need_num > 0:
                    res_id = res_row.get("Id")
                    res_sno = res_row.get("StockNo")
                    res_fwi = res_row.get("FromWarehouseId")
                    res_fwn = res_row.get("FromWarehouseName")
                    res_mpi = res_row.get("MainPartId")
                    res_mpn = res_row.get("MainPartName")
                    res_su = res_row.get("SupplierName")
                    res_oi = res_row.get("OwnerId")
                    res_on = res_row.get("OwnerName")
                    res_osn = res_row.get("OwnerShortName")
                    res_wi = res_row.get("WarehouseId")
                    res_mi = res_row.get("MaterialId")
                    res_mn = res_row.get("MaterialName")
                    res_mt = res_row.get("MaterialType")
                    res_mtc = res_row.get("MaterialTypeCode")
                    res_io = res_row.get("InOrOut")
                    res_as = res_row.get("AuditState")
                    res_st = res_row.get("StockType")
                    res_at = res_row.get("AuditTime")
                    res_sn = res_row.get("StockNum")
                    res_mp = res_row.get("StockPrice")
                    res_tx = res_row.get("TaxPoint")

                    print("单据时间：" + res_at.strftime("%Y-%m-%d"))
                    print("单据类型：" + res_st)
                    print("单据数量：" + str(res_sn))

                    # 计算仓库库龄
                    if need_num > 0:
                        # 需要重新计算仓库库龄的, +
                        if res_io == 0:
                            if need_num > res_sn:
                                mi_num = res_sn
                                need_num -= res_sn
                                if cu_sn > res_sn:
                                    cu_sn -= res_sn
                                else:
                                    cu_sn = 0
                            else:
                                mi_num = need_num
                                need_num = 0
                            print("本次入后所需库存量：" + str(cu_sn))
                            print("本次入后库龄库存所需量：" + str(need_num))
                            print("本次入实际消耗库龄库存：" + str(mi_num))
                            if res_st == "IN3":
                                cur.execute(sql_find_at, [res_mi, res_at])
                                sql_res = cur.fetchall()
                                if sql_res:
                                    in_at = sql_res[0].get("FnStockTime")
                                else:
                                    in_at = res_at
                            else:
                                in_at = res_at
                            params_in = [
                                res_id,
                                res_sno,
                                res_mpi,
                                res_mpn,
                                res_su,
                                res_oi,
                                res_on,
                                res_osn,
                                res_st,
                                res_io,
                                res_wi,
                                res_fwi,
                                res_fwn,
                                res_mi,
                                res_mn,
                                res_mt,
                                res_mtc,
                                res_mp,
                                res_tx,
                                in_at,
                                res_at,
                                res_sn,
                                mi_num,
                            ]
                            sql_in = (
                                "INSERT INTO wh_stockagebase (Id, StockNo, MainPartId, MainPartName, "
                                "SupplierName, OwnerId, OwnerName, OwnerShortName, StockType, InOrOut, "
                                "WarehouseId, FromWarehouseId, FromWarehouseName, MaterialId, "
                                "MaterialName, MaterialType, MaterialTypeCode, StockPrice, TaxPoint, "
                                "FnStockTime, StockTime, StockNum, SurplusNum) "
                                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                                "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                            )
                            cur.execute(sql_in, params_in)

                        # 需要重新计算仓库库龄的, -
                        if res_io == 1:
                            cu_sn += res_sn
                            print("本次出后所需库存量：" + str(cu_sn))
                            params_ot = [
                                res_id,
                                res_sno,
                                res_mpi,
                                res_mpn,
                                res_su,
                                res_oi,
                                res_on,
                                res_osn,
                                res_st,
                                res_io,
                                res_wi,
                                res_fwi,
                                res_fwn,
                                res_mi,
                                res_mn,
                                res_mt,
                                res_mtc,
                                res_mp,
                                res_tx,
                                res_at,
                                res_sn,
                            ]
                            sql_ot = (
                                "INSERT INTO wh_stockagebase (Id, StockNo, MainPartId, MainPartName, "
                                "SupplierName, OwnerId, OwnerName, OwnerShortName, StockType, InOrOut, "
                                "WarehouseId, FromWarehouseId, FromWarehouseName, MaterialId, "
                                "MaterialName, MaterialType, MaterialTypeCode, StockPrice, TaxPoint, "
                                "StockTime, StockNum) "
                                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                                "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                            )
                            cur.execute(sql_ot, params_ot)
                            con.commit()
                        print("仓库库龄剩余需扣库存量：" + str(need_num))
            use_time = datetime.now() - loop_time
            use_times = use_time.total_seconds()
            loop_time = datetime.now()
            print("本物料、业务主体耗时： ", use_times, " 秒！")
    # 循环所有出库单
    # 清理临时数据
    # sql_tru_1 = 'truncate table wh_stockagerelation;'
    # sql_tru_2 = 'truncate table wh_stockdetailinless;'
    # cur.execute(sql_tru_1)
    # cur.execute(sql_tru_2)
    # sql_rows_ot = ('SELECT DISTINCT MaterialId, MaterialName, OwnerId, OwnerName FROM wh_stockagebase WHERE '
    #                'InOrOut = 1;')
    # # AND MaterialId = \'MI0000003287\' AND OwnerId= \'ON9999999998\'
    # cur.execute(sql_rows_ot)
    # res_rows_ot = cur.fetchall()
    # for row_ot in res_rows_ot:
    #     r_mi = row_ot.get('MaterialId')
    #     r_mn = row_ot.get('MaterialName')
    #     r_oi = row_ot.get('OwnerId')
    #     r_on = row_ot.get('OwnerName')
    #     print('开始处理出库库存扣减！物料Id:{},货主Id:{}'.format(r_mi, r_oi))
    #     # 查询单条出库单数据
    #     sql_row_ot = ('SELECT StockNo, MaterialId, OwnerId, StockTime, BackNum '
    #                   'FROM wh_stockagebase '
    #                   'WHERE InOrOut = 1 '
    #                   'AND MaterialId = %s '
    #                   'AND OwnerId = %s '
    #                   'ORDER BY StockTime;')
    #     cur.execute(sql_row_ot, [r_mi, r_oi])
    #     res_row_ot = cur.fetchall()
    #     for ot in res_row_ot:
    #         ot_no = ot.get('StockNo')
    #         ot_st = ot.get('StockTime')
    #         ot_sn = ot.get('BackNum')
    #         print('开始处理出库单：{}'.format(ot_no))
    #         # 扣减库存
    #         sql_rows_in = ('SELECT Id, StockNo, StockNum, SurplusNum '
    #                        'FROM wh_stockagebase '
    #                        'WHERE InOrOut = 0 '
    #                        'AND MaterialId = %s '
    #                        'AND OwnerId = %s '
    #                        'AND SurplusNum != 0 '
    #                        'ORDER BY StockTime '
    #                        'LIMIT 1;')
    #         sql_up_in = 'UPDATE wh_stockagebase SET SurplusNum = (SurplusNum - %s) WHERE Id = %s;'
    #         sql_no_in = ('INSERT INTO wh_stockdetailinless(MaterialId,MaterialName,OwnerId,OwnerName,LessNum)'
    #                      'VALUES(%s, %s, %s, %s, %s) ON DUPLICATE KEY UPDATE MaterialId=%s,MaterialName=%s,'
    #                      'OwnerId=%s,OwnerName=%s,LessNum=LessNum+%s;')
    #         sql_in_out = ('INSERT INTO wh_stockagerelation(MaterialId, MaterialName, OwnerId, OwnerName, '
    #                       'OutStockNo, InStockNo, SurplusNum) VALUES(%s, %s, %s, %s, %s, %s, %s)')
    #         while ot_sn > 0:
    #             cur.execute(sql_rows_in, [r_mi, r_oi])
    #             res_rows_in = cur.fetchall()
    #             if res_rows_in:
    #                 in_id = res_rows_in[0].get('Id')
    #                 in_no = res_rows_in[0].get('StockNo')
    #                 in_sn = res_rows_in[0].get('SurplusNum')
    #                 if in_sn >= ot_sn:
    #                     cur.execute(sql_up_in, [ot_sn, in_id])
    #                     cur.execute(sql_in_out, [r_mi, r_mn, r_oi, r_on, ot_no, in_no, ot_sn])
    #                     print('扣减了入库单：{},库存：{}个！'.format(in_no, ot_sn))
    #                     ot_sn = 0
    #                     con.commit()
    #                 elif in_sn < ot_sn:
    #                     cur.execute(sql_up_in, [in_sn, in_id])
    #                     cur.execute(sql_in_out, [r_mi, r_mn, r_oi, r_on, ot_no, in_no, in_sn])
    #                     print('扣减了入库单：{},库存：{}个！'.format(in_no, in_sn))
    #                     ot_sn -= in_sn
    #                     con.commit()
    #             else:
    #                 cur.execute(sql_no_in, [r_mi, r_mn, r_oi, r_on, ot_sn, r_mi, r_mn, r_oi, r_on, ot_sn])
    #                 con.commit()
    #                 ot_sn = -1
    con.commit()
    total_time = datetime.now() - cu_time
    total_times = total_time.total_seconds()
    print("总计耗时： ", total_times, " 秒！")
except Exception as e:
    con.rollback()
    print(e)
finally:
    cur.close()
    con.close()
