#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw 
# @generate at 2023/9/6 13:05

#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# @author by wangcw
# @generate at 2023/9/1 09:17

import mysql.connector
import configparser
from datetime import datetime

config = configparser.ConfigParser()
config.read('db.conf')

my_host = config.get('whcenter', 'host')
my_database = config.get('whcenter', 'database')
my_user = config.get('whcenter', 'user')
my_password = config.get('whcenter', 'password')

con = mysql.connector.connect(
    host=my_host,
    user=my_user,
    password=my_password,
    database=my_database
)
cu_time = datetime.now()
cu_date = cu_time.date()
loop_time = datetime.now()

try:
    cur = con.cursor(dictionary=True)

    # 循环所有出库单
    sql_rows_ot = 'SELECT DISTINCT MaterialId, MaterialName, OwnerId, OwnerName FROM wh_stockagebase WHERE InOrOut = 1;'
    cur.execute(sql_rows_ot)
    res_rows_ot = cur.fetchall()
    for row_ot in res_rows_ot:
        r_mi = row_ot.get('MaterialId')
        r_mn = row_ot.get('MaterialName')
        r_oi = row_ot.get('OwnerId')
        r_on = row_ot.get('OwnerName')
        # 查询单条出库单数据
        sql_row_ot = ('SELECT MaterialId, OwnerId, StockTime, StockNum '
                      'FROM wh_stockagebase '
                      'WHERE InOrOut = 1 '
                      'AND MaterialId = %s '
                      'AND OwnerId = %s '
                      'ORDER BY StockTime;')
        cur.execute(sql_row_ot, [r_mi, r_oi])
        res_row_ot = cur.fetchall()
        for ot in res_row_ot:
            ot_st = ot.get('StockTime')
            ot_sn = ot.get('StockNum')
            # 扣减库存
            sql_rows_in = ('SELECT Id, StockNum, SurplusNum '
                           'FROM wh_stockagebase '
                           'WHERE InOrOut = 0 '
                           'AND MaterialId = %s '
                           'AND OwnerId = %s '
                           'AND SurplusNum != 0 '
                           'ORDER BY StockTime '
                           'LIMIT 1;')
            sql_up_in = 'UPDATE wh_stockagebase SET SurplusNum = (SurplusNum - %s) WHERE Id = %s;'
            sql_no_in = ('INSERT INTO wh_stockdetailinless(MaterialId,MaterialName,OwnerId,OwnerName,LessNum)'
                         'VALUES(%s, %s, %s, %s, %s) ON DUPLICATE KEY UPDATE MaterialId=%s,MaterialName=%s,'
                         'OwnerId=%s,OwnerName=%s,LessNum=LessNum+%s;')

            while ot_sn > 0:
                cur.execute(sql_rows_in, [r_mi, r_oi])
                res_rows_in = cur.fetchall()
                if res_rows_in:
                    in_id = res_rows_in[0].get('Id')
                    in_sn = res_rows_in[0].get('SurplusNum')
                    if in_sn >= ot_sn:
                        cur.execute(sql_up_in, [ot_sn, in_id])
                    elif in_sn < ot_sn:
                        ot_sn -= in_sn
                        cur.execute(sql_up_in, [in_sn, in_id])
                else:
                    cur.execute(sql_no_in, [r_mi, r_mn, r_oi, r_on, ot_sn, r_mi, r_mn, r_oi, r_on, ot_sn])
                    ot_sn = -1
                    print('物料Id：{},主体Id：{}，入库库存不足！'.format(r_mi, r_oi))
    con.commit()
    total_time = datetime.now() - cu_time
    total_times = total_time.total_seconds()
    print('总计耗时： ', total_times, ' 秒！')
except Exception as e:
    con.rollback()
    print(e)
finally:
    cur.close()
    con.close()
